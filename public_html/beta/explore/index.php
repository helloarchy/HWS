<!DOCTYPE HTML>
<html lang="en">
<head>
    <!--
    Explorer - index.php
    This file defines the explore page, where users can browse and filter through
    all products sold on the site, specifically fabrics.
    @author Robert Hardy
    @since 2018-08-02
    Part of the Hardy'sWebSystem project.
    -->
    <title>Jones &amp Hicks - Explore</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- JavaScript Files -->
    <script src="../_global/script/class/NavSite.js"></script>
    <script src="../_global/script/class/NavAccount.js"></script>
    <script src="../_global/script/class/NavBasket.js"></script>
    <script src="../_global/script/class/NavFooter.js"></script>
    <script src="script/ProductFilter.js"></script>
    <script src="script/ProductSort.js"></script>
    <script src="script/ProductList.js"></script>
    <script src="script/ProductItem.js"></script>
    <script src="script/ProductShowNext.js"></script>

    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-site.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-account.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/header.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/default.css">
    <link rel="stylesheet" type="text/css" href="style/_main.css">
    <link rel="stylesheet" type="text/css" href="style/filter.css">
    <link rel="stylesheet" type="text/css" href="style/sort.css">
    <link rel="stylesheet" type="text/css" href="style/product_list.css">
    <link rel="stylesheet" type="text/css" href="style/product_item.css">

    <?php
    // Initialise everything from init.
    require_once __DIR__ . '/../../../application/core/initialise.php';
    ?>
</head>
<body>
<!-- Layout the grid -->
<div id="grid-page">

    <!-- Header -->
    <header class="border grid-box" id="header">
        <div class="border" id="header-title-account-basket-row">
            <h1 id="header-title">
                Jones &amp Hicks
            </h1>
            <div class="border inner-box" id="nav-account-container">
                Account
            </div>
            <div class="border inner-box" id="nav-basket-container">
                Basket
            </div>
        </div>
        <!-- Site Nav Bar -->
        <div class="border" id="header-nav-site-row">
            <nav class="border inner-box" id="nav-site-container">
                <!-- Populate by script -->
            </nav>
            <div class="border inner-box spacer-nav">
                <i>Spacer</i>
            </div>
        </div>
    </header>

    <!-- Sidebar Left -->
    <div class="border grid-box" id="sidebar-left">
        Sidebar (Left)
        <div class="border inner-box" id="filter-container">
            Filter Options
        </div>
    </div>

    <!-- Product List -->
    <main class="border grid-box" id="main">
        Main
        <!-- Sort Options -->
        <div class="border inner-box" id="sort-container">
            Sort Options
        </div>
        Product List
        <!-- Product List -->
        <div class="border inner-box" id="grid-products">
            <p>A fabric to be...</p>
            <p>A fabric to be...</p>
            <p>A fabric to be...</p>
            <p>A fabric to be...</p>
            <p>A fabric to be...</p>
            <p>A fabric to be...</p>
            <p>A fabric to be...</p>
            <p>A fabric to be...</p>
            <p>A fabric to be...</p>
        </div>
        <!-- Show Next -->
        <div class="border inner-box" id="show-next-container">
            Show next button
        </div>
    </main>

    <!-- Sidebar Right -->
    <div class="border grid-box" id="sidebar-right">
        Sidebar (Right)
    </div>

    <!-- Footer -->
    <footer class="border grid-box" id="footer">
        Footer
        <div class="border inner-box" id="nav-footer-container">
            Footer nav
        </div>
    </footer>

    <!-- Scripts for handling the above -->
    <script>
        /* Header Navigation */
        let navSite = new NavSite("nav-site-container");
        let navAccount = new NavAccount("nav-account-container");
        let navBasket = new NavBasket("nav-basket-container");

        /* Create a new product list for the whole page */
        let product_list = new ProductList("grid-products");

        /* Set up sort for ordering the list contents */
        let sort = new ProductSort("sort-container", product_list);

        /* Set up 'Show Next' */
        let showNext = new ProductShowNext("show-next-container", sort, product_list);

        /* Set up filter for refining the list */
        let filter = new ProductFilter("filter-container", product_list, sort, showNext);

        /* Footer Navigation */
        let footer = new NavFooter("nav-footer-container");
    </script>
</div>
</body>

