/**
 * Product Item - ProductItem.ts
 * This file defines a single product item for displaying in the product list.
 *
 * @author Robert Hardy
 * @since 2018-08-07
 * Part of the Hardy'sWebSystem project.
 */
class ProductItem {
    private parentID: string;

    constructor(parentID: string) {
        this.parentID = parentID;
    }
}