/**
 * Show Next - ProductShowNext.ts
 * This file defines the show next button for the list of products, it handles
 * creation of HTML, event listeners, and handling of events using ProductList.ts
 * for symbiotic control.
 *
 * @author Robert Hardy
 * @since 2018-08-29
 * Part of the Hardy'sWebSystem project.
 */
class ProductShowNext {
    private readonly parentNode: HTMLElement;
    private product_sort;
    private product_list;


    /**
     *
     * @param parentID
     * @param product_sort
     * @param product_list
     */
    constructor(parentID: string, product_sort, product_list) {
        this.parentNode = document.getElementById(parentID);
        this.product_sort = product_sort;
        this.product_list = product_list;

        /* See if anymore results available via AJAX and handle reply. */
        this.hasNext(this.product_sort.getN());
    }


    /**
     * See if there are anymore results.
     */
    public hasNext(n_results) {
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/ProductList.php",
            data: {
                N: n_results,
                hasNext: "true"
            },
            type: "POST",
        }).done(function (reply) {
                /* if there are more results, create HTML button, else clear it. */
                let parsedReply = JSON.parse(reply);
                if (parsedReply === true) {
                    that.createHTML();
                } else {
                    that.clearHTML();
                }
            }
        );
    }


    /**
     * Create the button and event handlers now we know more results are available.
     */
    private createHTML() {
        /* Clear the canvas of any existing button */
        this.clearHTML();

        /* Create the button */
        let button = document.createElement('button');
        button.setAttribute('id', 'show-next-button');
        button.innerText = "show next";

        /* Append everything */
        this.parentNode.appendChild(button);

        /* Add event listener */
        let that = this;
        let button_elem = document.getElementById('show-next-button');
        button_elem.addEventListener('click', function () {
            /* Get the next results, then see if more results available before
            * showing the button. */
            let n_results = that.product_sort.getN();
            console.log("Show next button clicked! n_results = " + n_results);
            that.product_list.getNext(n_results);
            that.hasNext(n_results);
        });
    }


    /**
     * Get rid of the button now no more results are available.
     */
    private clearHTML() {
        this.parentNode.innerHTML = "";
    }
}