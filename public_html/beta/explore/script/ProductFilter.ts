/**
 * Filter for Products - ProductFilter.ts
 * This file defines the ability for a user to filter the product list, by various
 * parameters such as type, colour, price, etc.
 *
 * @author Robert Hardy
 * @since 2018-08-07
 * Part of the Hardy'sWebSystem project.
 */
class ProductFilter {
    private readonly parentNode: HTMLElement;
    private product_list;
    private product_sort;
    private showNext;
    private readonly brand_choice_id: string;
    private readonly collection_choice_id: string;
    private readonly price_choice_id: string;
    private readonly colour_choice_id: string;
    private readonly width_choice_id: string;
    private readonly pattern_choice_id: string;
    private readonly type_choice_id: string;
    private readonly application_choice_id: string;

    /**
     * Create a new filter panel for products
     * @param parentID
     * @param productList
     * @param productSort
     * @param showNext
     */
    constructor(parentID: string, productList, productSort, showNext) {
        this.parentNode = document.getElementById(parentID);
        this.product_list = productList;
        this.product_sort = productSort;
        this.showNext = showNext;

        /* Set choice ID's */
        this.brand_choice_id = "filter-choice-container-brand";
        this.collection_choice_id = "filter-choice-container-collection";
        this.price_choice_id = "filter-choice-container-price";
        this.colour_choice_id = "filter-choice-container-colour";
        this.width_choice_id = "filter-choice-container-width";
        this.pattern_choice_id = "filter-choice-container-pattern";
        this.type_choice_id = "filter-choice-container-type";
        this.application_choice_id = "filter-choice-container-application";

        /* Create category headings */
        this.createHeadingHTML("brand", this.brand_choice_id);
        this.createHeadingHTML("collection", this.collection_choice_id);
        this.createHeadingHTML("price", this.price_choice_id);
        this.createHeadingHTML("colour", this.colour_choice_id);
        this.createHeadingHTML("width", this.width_choice_id);
        this.createHeadingHTML("pattern", this.pattern_choice_id);
        this.createHeadingHTML("type", this.type_choice_id);
        this.createHeadingHTML("application", this.application_choice_id);

        /* Populate choices - get the choices and then create HTML for them  */
        this.getAllFilters();
    }


    /**
     * Create the category container and heading.
     * @param title
     * @param choice_id
     */
    private createHeadingHTML(title: string, choice_id: string) {
        /* Create container */
        let container = document.createElement('div');
        container.setAttribute('class',
            'filter-category-container');

        /* Create heading container */
        let heading_container = document.createElement('div');
        heading_container.setAttribute('class',
            'filter-category-heading');

        /* Create heading title and text */
        let heading_title = document.createElement('p');
        heading_title.setAttribute('class',
            'filter-category-title');
        let heading_title_text = document.createTextNode(title);

        /* Creating heading drop down arrow and text */
        let heading_drop_down = document.createElement('p');
        heading_drop_down.setAttribute('class',
            'filter-category-drop-down');
        let heading_drop_text_id = 'filter-category-drop-down-' + title;
        heading_drop_down.setAttribute('id',
            heading_drop_text_id);
        let heading_drop_down_text = document.createTextNode("\u2227");

        /* Create choice div (for hiding) */
        let choice_container = document.createElement('div');
        choice_container.setAttribute('class',
            'filter-category-choice-container');
        choice_container.setAttribute('id', choice_id);

        /* Append everything */
        heading_title.appendChild(heading_title_text);
        heading_container.appendChild(heading_title);
        heading_drop_down.appendChild(heading_drop_down_text);
        heading_container.appendChild(heading_drop_down);
        container.appendChild(heading_container);
        container.appendChild(choice_container);
        this.parentNode.appendChild(container);

        /* Add event handler for drop down */
        let drop_arrow = document.getElementById(heading_drop_text_id);
        heading_container.addEventListener('click', function () {
            /* Toggle - if hidden then show, else hide. */
            drop_arrow.innerHTML = "";
            let new_drop_text = null;
            if (choice_container.style.display === 'none') {
                choice_container.style.display = 'block';
                new_drop_text = document.createTextNode("\u2227");
                drop_arrow.appendChild(new_drop_text);
            } else {
                choice_container.style.display = 'none';
                new_drop_text = document.createTextNode("\u2228");
                drop_arrow.appendChild(new_drop_text);
            }
        });
    }


    /**
     * Get every possible filter at first page load via AJAX.
     */
    private getAllFilters() {
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/Filter.php",
            data: {
                getAllFilters: "true"
            },
            type: "POST",
        }).done(function (reply) {
                that.parseAllFilters(reply);
            }
        );
    }


    /**
     * Populate choice containers using the column values
     * @param ajax_response
     */
    private parseAllFilters(ajax_response) {
        let all_filters = JSON.parse(ajax_response);
        // Brand
        this.createAllChoiceCheckBox(this.brand_choice_id,
            "brand", all_filters.columns.brands);
        // Collection
        this.createAllChoiceCheckBox(this.collection_choice_id,
            "collection", all_filters.columns.collections);
        // Price - specific method for slider
        this.createAllChoiceRange("price_current", this.price_choice_id,
            all_filters.columns.current_prices);
        // Colour
        this.createAllChoiceCheckBox(this.colour_choice_id,
            "colour_hardys", all_filters.columns.colours);
        // Width
        this.createAllChoiceCheckBox(this.width_choice_id,
            "width", all_filters.columns.widths);
        // Pattern
        this.createAllChoiceCheckBox(this.pattern_choice_id,
            "pattern", all_filters.columns.patterns);
        // Type
        this.createAllChoiceCheckBox(this.type_choice_id,
            "type", all_filters.columns.types);
        // Application
        this.createAllChoiceCheckBox(this.application_choice_id,
            "application", all_filters.columns.applications);
    }


    /**
     * Iterate through all values in the column, calling createChoiceCheckBoxHTML
     * to make the html and event handler for the choice, appending it to the
     * container.
     * @param choice_container
     * @param column_name
     * @param column
     */
    private createAllChoiceCheckBox(choice_container, column_name, column) {
        for (let i = 0; i < column.length; i++) {
            this.createChoiceCheckBoxHTML(column_name, choice_container, column[i]);
        }
    }


    /**
     * Iterate through all values in the column, finding the min and max values
     * creating the slider and other html. Event handlers are also attached.
     * TODO: Implement a multi-range slider when they become available (currently
     * TODO: only available via JQueryUI and other dependencies.
     * @param column
     * @param choice_container
     * @param column_values

     */
    private createAllChoiceRange(column, choice_container, column_values) {
        let parent = document.getElementById(choice_container);
        let min = Math.min(...column_values);
        let max = Math.max(...column_values);

        /* Create container */
        let container = document.createElement('div');
        container.setAttribute('class',
            'filter-choice-range-container');

        /* Create label */
        let title = document.createElement('p');
        title.setAttribute('class',
            'filter-choice-title');
        let title_text = document.createTextNode("Maximum per metre");

        /* Create range slider */
        let range = document.createElement('input');
        range.setAttribute('type', 'range');
        range.setAttribute('min', min.toString());
        range.setAttribute('max', max.toString());
        range.setAttribute('value', max.toString()); // Set at full
        range.setAttribute('class', 'filter-choice-range');
        range.setAttribute('id', 'filter-choice-range-price');

        /* Show range value  */
        let value = document.createElement('p');
        value.setAttribute('class',
            'filter-choice-range-value');
        let formatted_value = Number(range.value).toFixed(2);
        value.innerHTML = "Up to £" + formatted_value;

        /* Append everything */
        title.appendChild(title_text);
        container.appendChild(title);
        container.appendChild(range);
        container.appendChild(value);
        parent.appendChild(container);

        /* Add event listeners */
        let that = this;
        let range_slider = <HTMLInputElement>document.getElementById(
            'filter-choice-range-price');

        /* Tell Product List what the value is */
        this.product_list.setPrice_Current(Number(range_slider.value));

        /* Show the value as the user slides the head so they know which value
        * to stop on */
        range_slider.addEventListener('input', function() {
            let formatted_value = Number(this.value).toFixed(2);
            value.innerHTML = "Up to £" + formatted_value;
        });

        /* When the user has finished setting value, update the product list */
        range_slider.addEventListener('change', function () {
            console.log("Range " + column + " set to " + this.value);
            that.product_list.setRangeFilter(column, this.value, that.product_sort.getN());
            /* update show next */
            that.showNext.hasNext(that.product_sort.getN());
        });
    }


    /**
     * Create a choice and append it to the parent.
     * @param column
     * @param parent_id
     * @param choice_value
     */
    private createChoiceCheckBoxHTML(column, parent_id, choice_value) {
        /* Create container */
        let container = document.createElement('div');
        container.setAttribute('class',
            'filter-choice-check-container');

        /* Create label */
        let title = document.createElement('p');
        title.setAttribute('class',
            'filter-choice-title');
        let title_text = document.createTextNode(choice_value);

        /* Create check box */
        let check_box = document.createElement('input');
        check_box.setAttribute('class', 'filter-choice-check-box');
        check_box.setAttribute('id', 'filter-choice-check-');
        check_box.setAttribute('type', 'checkbox');

        /* Append everything */
        title.appendChild(title_text);
        container.appendChild(title);
        container.appendChild(check_box);
        let parent = document.getElementById(parent_id);
        parent.appendChild(container);

        /* Add event listener */
        let that = this;
        check_box.addEventListener('change', function () {
            that.optionCheckChange(column, choice_value, this.checked);
        });
    }


    /**
     * Handle when a choice is checked or unchecked.
     * @param column
     * @param choice
     * @param isChecked
     */
    private optionCheckChange(column, choice, isChecked) {
        let n_results = this.product_sort.getN();
        if (isChecked) {
            console.log(column + " choice " + choice + " is now checked.");
            this.product_list.setCheckFilter(column, choice, true, n_results);
        } else {
            console.log(column + " choice " + choice + " is now unchecked.");
            this.product_list.setCheckFilter(column, choice, false, n_results);
        }
        /* update show next */
        this.showNext.hasNext(n_results);
    }
}