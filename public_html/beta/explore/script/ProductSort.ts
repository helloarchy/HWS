/**
 * Product Sort - ProductSort.ts
 * This file defines the ability for a user to sort the product list to dictate
 * the ordering of the displayed items.
 *
 * @author Robert Hardy
 * @since 2018-08-07
 * Part of the Hardy'sWebSystem project.
 */
class ProductSort {
    private readonly parentNode: HTMLElement;
    private product_list;
    private readonly column_select_values: string[][];
    private select_column_node: HTMLInputElement;
    private select_asc_desc_node: HTMLInputElement;
    private select_n_results_node: HTMLInputElement;
    private n_results;

    /**
     * Create a new sort panel for the products
     * @param parentID
     * @param productList
     */
    constructor(parentID: string, productList) {
        this.parentNode = document.getElementById(parentID);
        this.product_list = productList;
        this.column_select_values = [
            ["Reduction", "reduction"],
            ["Brand", "brand"],
            ["Collection", "collection"],
            ["Price", "price_current"],
            ["Colour", "colour"],
            ["Width", "width"],
            ["Pattern", "pattern"],
            ["Type", "type"],
        ];

        /* Make the select (drop down list) */
        this.createColumnSelect();

        /* Populate it with the choices */
        for (let i = 0; i < this.column_select_values.length; i++) {
            this.createSelectOption(
                this.column_select_values[i][0],
                this.column_select_values[i][1],
                this.select_column_node
            );
        }

        /* Create Ascending or Descending select */
        this.createAscDescSelect();

        /* Populate it with Ascending and Descending options */
        this.createSelectOption("Ascending", "asc",
            this.select_asc_desc_node);
        this.createSelectOption("Descending", "desc",
            this.select_asc_desc_node);

        /* Create N results select */
        this.createNSelect();

        /* Populate it with choices */
        for (let i = 1; i < 11; i++) {
            this.createSelectOption(i.toString(), i, this.select_n_results_node);
        }

        /* Set default values for each select */
        this.select_column_node.value = "reduction";
        this.select_asc_desc_node.value = "asc";
        this.select_n_results_node.value = "5";
        this.setN(5);

        /* TODO: Sort out N results select. Call product list by default. */

        /* TODO: Make me a SELECT */
        //this.product_list.getInitial(this.n_results);
    }


    /**
     *
     */
    private createColumnSelect() {
        /* Create label for the select */
        let title = document.createElement('p');
        title.setAttribute('class','sort-title');
        title.setAttribute('id', 'sort-title-sort');
        let title_text = document.createTextNode("Sort: ");

        /* Create select */
        let select = document.createElement('select');
        select.setAttribute('class', 'sort-select ' +
            'sort-select-column');
        select.setAttribute('id', 'sort-select-column');

        /* Append to parent  */
        title.appendChild(title_text);
        this.parentNode.appendChild(title);
        this.parentNode.appendChild(select);

        /* Update reference for options */
        this.select_column_node =
            <HTMLInputElement>document.getElementById('sort-select-column');

        /* Add event handler */
        let that = this;
        select.addEventListener('change', function () {
            console.log("Sort option changed to " + that.select_column_node.value);
            that.product_list.setSort(
                that.select_column_node.value,
                that.select_asc_desc_node.value,
                that.n_results
            );
        });
    }


    /**
     *
     */
    private createAscDescSelect() {
        /* Create select */
        let select = document.createElement('select');
        select.setAttribute('class', 'sort-select ' +
            'sort-select-asc-desc');
        select.setAttribute('id', 'sort-select-asc-desc');

        /* Append to parent  */
        this.parentNode.appendChild(select);

        /* Update reference for options */
        this.select_asc_desc_node =
            <HTMLInputElement>document.getElementById('sort-select-asc-desc');

        /* Add event handler */
        let that = this;
        select.addEventListener('change', function () {
            console.log("Sort asc/desc changed to " + that.select_asc_desc_node.value);
            that.product_list.setSort(
                that.select_column_node.value,
                that.select_asc_desc_node.value,
                that.n_results
            );
        });
    }


    /**
     *
     */
    private createNSelect() {
        /* Create label for the select */
        let title = document.createElement('p');
        title.setAttribute('class','sort-title');
        title.setAttribute('id', 'sort-title-show');
        let title_text = document.createTextNode("Show: ");

        /* Create select */
        let select = document.createElement('select');
        select.setAttribute('class', 'sort-select ' +
            'sort-select-n-results');
        select.setAttribute('id', 'sort-select-n-results');

        /* Append to parent  */
        title.appendChild(title_text);
        this.parentNode.appendChild(title);
        this.parentNode.appendChild(select);

        /* Update reference for options */
        this.select_n_results_node =
            <HTMLInputElement>document.getElementById('sort-select-n-results');

        /* Add event handler */
        let that = this;
        select.addEventListener('change', function () {
            console.log("Sort: N results changed to " + that.select_n_results_node.value);
            that.setN(this.value);
        });
    }


    /**
     * Create an option for the select.
     * @param title
     * @param value
     * @param select
     */
    private createSelectOption(title, value, select) {
        /* Create the option */
        let option = document.createElement('option');
        option.setAttribute('value', value);
        option.setAttribute('id', value);
        option.setAttribute('class', 'sort-option');

        /* Create the text */
        let option_text = document.createTextNode(title);

        /* Append everything */
        option.appendChild(option_text);
        select.appendChild(option);
    }


    /**
     * Get the N number of results
     */
    public getN() {
        return this.n_results;
    }


    /**
     * Set the N results and reflect the value change across the product page.
     * @param n_results
     */
    public setN(n_results) {
        this.n_results = n_results;
        this.product_list.getInitial(n_results);
        /* TODO: CALL METHODS IN OTHERS TO REFLECT CHANGE */
    }
}