/**
 * Product List - ProductList.ts
 * This file defines the list of product items which via ProductSort, are then
 * displayed to the user. Filtering of the list is achieved via ProductFilter.
 *
 * @author Robert Hardy
 * @since 2018-08-07
 * Part of the Hardy'sWebSystem project.
 */
class ProductList {
    private parentNode: HTMLElement;
    private readonly brand: Array<string>;
    private readonly collection: Array<string>;
    private price_current: number;
    private readonly colour_hardys: Array<string>;
    private readonly width: Array<string>;
    private readonly pattern: Array<string>;
    private readonly type: Array<string>;
    private readonly application: Array<string>;
    private sort: string[];


    /**
     * Create a new product list.
     * @param parentID
     */
    constructor(parentID: string) {
        this.parentNode = document.getElementById(parentID);

        /* Initialise filter arrays (range values initialised elsewhere */
        this.brand = Array();
        this.collection = Array();
        this.colour_hardys = Array();
        this.width = Array();
        this.pattern = Array();
        this.type = Array();
        this.application = Array();

        /* Set to reduction by default */
        this.sort = ["reduction", "asc"];
    }


    /**
     * Populate the list upon first page load. Listing the products with the most
     * discount first.
     */
    public getInitial(n_results) {
        /* Empty the container ready for filling */
        this.parentNode.innerHTML = "";
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/ProductList.php",
            data: {
                N: n_results,
                batch: "first",
                filtered_sorted: "false"
            },
            type: "POST",
        }).done(function (reply) {
                that.createAllItems(reply);
            }
        );
    }


    /**
     * Populate the list with filtered and/or sorted results.
     */
    public getFilteredSorted(n_results) {
        /* Empty the container ready for filling */
        this.parentNode.innerHTML = "";

        /* Send request and handle reply */
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/ProductList.php",
            data: {
                N: n_results,
                batch: "first",
                filtered_sorted: "true",
                brand: JSON.stringify(this.brand),
                collection: JSON.stringify(this.collection),
                price_current: this.price_current,
                colour_hardys: JSON.stringify(this.colour_hardys),
                width: JSON.stringify(this.width),
                pattern: JSON.stringify(this.pattern),
                type: JSON.stringify(this.type),
                application: JSON.stringify(this.application),
                sort: JSON.stringify(this.sort)
            },
            type: "POST"
        }).done(function (reply) {
                that.createAllItems(reply);
            }
        );
    }


    /**
     * Create product items using the results from the ajax.
     * @param ajax_reply
     */
    private createAllItems(ajax_reply) {
        /* Loop through the parsed reply and create an item
        using each columns value */
        let all_products = JSON.parse(ajax_reply);
        for (let i = 0; i < all_products.length; i++) {
            this.createItem(
                all_products[i].ufid,
                all_products[i].title,
                all_products[i].brand,
                all_products[i].price_current,
                all_products[i].price_original,
                all_products[i].reduction,
                all_products[i].image_main,
            );
        }
    }



    /**
     * Create the html and event handlers for a single product and add it to the
     * product list displayed to the user.
     * @param ufid
     * @param title
     * @param brand
     * @param price_current
     * @param price_original
     * @param price_reduction
     * @param image
     */
    private createItem(ufid, title, brand, price_current, price_original,
                       price_reduction, image) {
        /* Create container */
        let container = document.createElement('div');
        container.setAttribute('class',
            'product-item-container');
        container.setAttribute('id', 'product-item-' + ufid);

        /* Create image and append it. If image not found, use placeholder. */
        let product_img = document.createElement('div');
        if (!(image === null)) {
            product_img.style.backgroundImage =
                "url('../_assets/image/product/s/" + image + "')";
        } else {
            product_img.style.backgroundImage =
                "url('../_assets/image/error_product_default.png')";
        }
        product_img.setAttribute('class', 'product-item-image');
        container.appendChild(product_img);

        /* Create title */
        let product_title = document.createElement('p');
        product_title.setAttribute('class',
            'product-item-title');
        let title_text = document.createTextNode(title);
        product_title.appendChild(title_text);
        container.appendChild(product_title);

        /* Create brand subtitle */
        let product_brand = document.createElement('p');
        product_brand.setAttribute('class',
            'product-item-brand');
        let brand_text = document.createTextNode(brand);
        product_brand.appendChild(brand_text);
        container.appendChild(product_brand);

        /* Create the original price IF different from the current price */
        if (price_current < price_original) {
            let product_original_price = document.createElement('p');
            product_original_price.setAttribute('class',
                'product-item-original-price');
            let original_price_text = document.createTextNode("£" + price_original);
            product_original_price.appendChild(original_price_text);
            container.appendChild(product_original_price);
        }

        /* Create the current price */
        let product_current_price = document.createElement('p');
        product_current_price.setAttribute('class',
            'product-item-current-price');
        let current_price_text = document.createTextNode("£" + price_current + " \/ metre");
        product_current_price.appendChild(current_price_text);
        container.appendChild(product_current_price);

        /* Append container to parent */
        this.parentNode.appendChild(container);

        /* Create event listener - when product item clicked, redirect to the
         * product page, giving UFID in the GET. */
        let product_item = document.getElementById('product-item-' + ufid);
        product_item.addEventListener('click', function () {
            console.log("Product item " + title + " by " + brand + " clicked");
            let hex_ufid = parseInt(ufid).toString(16);
            window.location.href = "../product/index.php?ufid=0x" + hex_ufid;
        });
    }


    /**
     * Set the filter then trigger the product list to be updated.
     * @param column
     * @param value
     * @param n_results
     */
    public setRangeFilter(column: string, value: any, n_results: Number) {
        /* Determine the column the range is for, then update its value and
        * update product list. */
        if (column == "price_current") {
            this.price_current = value;
            this.getFilteredSorted(n_results);
        } else {
            throw new Error("Invalid range column name");
        }
    }


    /**
     * Set the filter then trigger the product list to be updated.
     * @param column
     * @param choice
     * @param value
     * @param n_results
     */
    public setCheckFilter(column: string, choice: string,
                          value: boolean, n_results: Number) {
        /* Determine the column to apply the filter to. */
        if (column == "brand") {
            this.updateCheckFilter(this.brand, choice, value, n_results);
        } else if (column == "collection") {
            this.updateCheckFilter(this.collection, choice, value, n_results);
        } else if (column == "colour_hardys") {
            this.updateCheckFilter(this.colour_hardys, choice, value, n_results);
        } else if (column == "width") {
            this.updateCheckFilter(this.width, choice, value, n_results);
        } else if (column == "pattern") {
            this.updateCheckFilter(this.pattern, choice, value, n_results);
        } else if (column == "type") {
            this.updateCheckFilter(this.type, choice, value, n_results);
        } else if (column == "application") {
            this.updateCheckFilter(this.application, choice, value, n_results);
        } else {
            throw new Error("SetCheckFilter: Invalid column name!");
        }
    }


    /**
     * Apply or remove a specific filter, then update the product list.
     * @param filter
     * @param choice
     * @param value
     * @param n_results
     */
    private updateCheckFilter(filter, choice, value, n_results: Number) {
        /* If value true, add the choice, else remove it if it exists. */
        //let exists = this.filters.indexOf(choice); // returns -1 if not
        if (value == true) {
            filter.push(choice); // push wont add it
        } else {
            filter.splice(
                filter.indexOf(choice), 1);
        }
        this.getFilteredSorted(n_results);
    }


    /**
     * Set the sort then trigger the product list to be updated.
     * @param column
     * @param asc_desc
     * @param n_results
     */
    public setSort(column, asc_desc, n_results) {
        this.sort = [column, asc_desc];
        this.getFilteredSorted(n_results);
    }


    /**
     *
     */
    public getNext(n_results) {
        /* Send request and handle reply */
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/ProductList.php",
            data: {
                N: n_results,
                batch: "next",
            },
            type: "POST",
        }).done(function (reply) {
                that.createAllItems(reply);
            }
        );
    }


    /**
     *
     * @param price
     */
    public setPrice_Current(price: number) {
        this.price_current = price;
    }
}