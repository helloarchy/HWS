<?php
/**
 * 404 Error page.
 * @author Robert Hardy
 * @since 2018-07-05
 *
 * This file defines a generic "404 not found" error message, to be included in
 * other pages when redirection fails.
 */
?>
<h1>404: Oops, that page cannot be found! :/</h1>
