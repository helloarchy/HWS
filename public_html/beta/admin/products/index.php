<?php

/**
 * Products Page - products.php
 * This file defines the admins products page, for managing and viewing products
 * within the whole system.
 * @author Robert Hardy
 * @since 2018-09-07
 * Part of the Hardy'sWebSystem project.
 */

/* Initialise */
require_once __DIR__ . '/../../../../application/core/initialise.php';

/* Make sure user is an admin, if not, redirect. */
$user = new User();
if ($user->hasPermission(2)) {
    /* User has required access level, produce page... */
    ?>
    <!DOCTYPE HTML>
    <html lang="en">
<head>
    <!--

    -->
    <title>Jones &amp Hicks - Admin - Product Manager</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- JavaScript Files -->
    <script src="../../_global/script/class/NavSite.js"></script>
    <script src="../../_global/script/class/NavAccount.js"></script>
    <script src="../../_global/script/class/NavBasket.js"></script>
    <script src="../../_global/script/class/NavFooter.js"></script>
    <script src="script/NewProduct.js"></script>

    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="../../_global/style/nav-site.css">
    <link rel="stylesheet" type="text/css" href="../../_global/style/nav-account.css">
    <link rel="stylesheet" type="text/css" href="../../_global/style/nav-side.css">
    <link rel="stylesheet" type="text/css" href="../../_global/style/header.css">
    <link rel="stylesheet" type="text/css" href="../../_global/style/default.css">
    <link rel="stylesheet" type="text/css" href="style/_main.css">
    <link rel="stylesheet" type="text/css" href="style/form.css">
    <link rel="stylesheet" type="text/css" href="style/type.css">

</head>
<body>

<!-- Layout the grid -->
<div id="grid-page">

    <!-- Header -->
    <header class="border grid-box" id="header">
        <div class="border" id="header-title-account-basket-row">
            <h1 id="header-title">
                Jones &amp Hicks
            </h1>
            <div class="border inner-box" id="nav-account-container">
                Account
            </div>
            <div class="border inner-box" id="nav-basket-container">
                Basket
            </div>
        </div>
        <!-- Site Nav Bar -->
        <div class="border" id="header-nav-site-row">
            <nav class="border inner-box" id="nav-site-container">
                <!-- Populate by script -->
            </nav>
            <div class="border inner-box spacer-nav">
                <i>Spacer</i>
            </div>
        </div>
    </header>

    <!-- Sidebar Left -->
    <div class="border grid-box" id="sidebar-left">
        manage products
        <!-- Actions Panel -->
        <div class="border nav-side" id="admin-product-actions-panel">
            <a href="index.php?action=stats">statistics</a>
            <a href="index.php?action=new">add new</a>
            <a href="index.php?action=all">manage all</a>
        </div>
    </div>

    <!-- Main -->
    <main class="border grid-box" id="main">
        <?php
        /* If any GET exists, display the relevant content from the includes,
        otherwise display product statistics by default. */
        if (Input::exists('get')) {
            if (Input::get('action') === 'stats') {
                include "include/stats.php";
            } else if (Input::get('action') === 'new') {
                /* Change to add new product form */
                include "include/new.php";
                if(Input::get('type') === 'interlining') {
                    /* Type is interlining */
                    echo "<script>let newProduct = new NewProduct(
                    'product-new-container', 'product-new-submit', 'interlining');
                    </script>";
                } else if (Input::get('type') === 'lining') {
                    /* Type is lining */
                    echo "<script>let newProduct = new NewProduct(
                    'product-new-container', 'product-new-submit', 'lining');
                    </script>";
                } else {
                    /* Otherwise type must be a fabric */
                    echo "<script>let newProduct = new NewProduct(
                    'product-new-container', 'product-new-submit', 'fabric');
                    </script>";
                }
            } else if (Input::get('action') === 'all') {
                include "include/all.php";
            }
        } else {
            include "include/stats.php";
        }
        ?>
    </main>

    <!-- Footer -->
    <footer class="border grid-box" id="footer">
        Footer
        <div class="border inner-box" id="nav-footer-container">
            Footer nav
        </div>
    </footer>

    <!-- Scripts for handling the above -->
    <script>
        /* Header Navigation */
        let navSite = new NavSite("nav-site-container");
        let navAccount = new NavAccount("nav-account-container");
        let navBasket = new NavBasket("nav-basket-container");

        /* Footer Navigation */
        let footer = new NavFooter("nav-footer-container");
    </script>
</div>
</body>
    <?php
} else {
    /* user does not have at least level 2 */
    Redirect::to(404);
}