/**
 * Add a New Product - NewProduct.ts
 * This file handles the new product form, and attempts to add it to the system
 * via AJAX, etc.
 *
 * @author Robert Hardy
 * @since 2018-09-10
 * Part of the Hardy'sWebSystem project.
 */
class NewProduct {
    private formNode: HTMLElement;
    private product_type: string;
    private errors: Array<string>;
    private reference;
    private title;
    private brand;
    private collection;
    private price_current;
    private price_original;
    private colour_brand;
    private colour_hardys;
    private description;
    private width;
    private pattern;
    private pattern_repeat;
    private pattern_match;
    private type;
    private retardancy;
    private application;
    private composition;
    private supplier;
    private available;
    private image;

    /**
     * Create a New Product handler.
     * @param formID
     * @param submitID
     * @param productType
     */
    constructor(formID: string, submitID: string, productType: string) {
        this.formNode = document.getElementById(formID);
        this.product_type = productType;
        this.errors = [];
        this.createEventListener(); // Listen for submit
    }


    /**
     *
     */
    private createEventListener() {
        let that = this;
        this.formNode.addEventListener('submit', function (event) {
            /* Stop page refresh */
            event.preventDefault();

            /* Get the form values for the relevant product type */
            if (that.product_type === "interlining") {
                that.getFormValuesInterlining();
            } else if (that.product_type === "lining") {
                that.getFormValuesLining();
            } else {
                that.getFormValuesFabric();
            }

            /* Get token then try creating a new product in with above values */
            that.getToken(false);
        });
    }


    /**
     *
     */
    private getFormValuesFabric() {
        /* Get form values */
        let reference = <HTMLInputElement>
            document.getElementById("new-form-reference");
        let title = <HTMLInputElement>
            document.getElementById("new-form-title");
        let brand = <HTMLInputElement>
            document.getElementById("new-form-brand");
        let collection = <HTMLInputElement>
            document.getElementById("new-form-collection");
        let price_current = <HTMLInputElement>
            document.getElementById("new-form-price-current");
        let price_original = <HTMLInputElement>
            document.getElementById("new-form-price-original");
        let colour_brand = <HTMLInputElement>
            document.getElementById("new-form-colour-brand");
        let colour_hardys = <HTMLInputElement>
            document.getElementById("new-form-colour-hardys");
        let description = <HTMLInputElement>
            document.getElementById("new-form-description");
        let width = <HTMLInputElement>
            document.getElementById("new-form-width");
        let pattern = <HTMLInputElement>
            document.getElementById("new-form-pattern");
        let pattern_repeat = <HTMLInputElement>
            document.getElementById("new-form-pattern-repeat");
        let pattern_match = <HTMLInputElement>
            document.getElementById("new-form-pattern-match");
        let type = <HTMLInputElement>
            document.getElementById("new-form-type");
        let retardancy = <HTMLInputElement>
            document.getElementById("new-form-retardancy");
        let application = <HTMLInputElement>
            document.getElementById("new-form-application");
        let composition = <HTMLInputElement>
            document.getElementById("new-form-composition");
        let supplier = <HTMLInputElement>
            document.getElementById("new-form-supplier");
        let available = <HTMLInputElement>
            document.getElementById("new-form-available");
        let image = <HTMLInputElement>
            document.getElementById("new-form-image");

        /* Assign values */
        this.reference = reference.value;
        this.title = title.value; // required
        this.brand = brand.value;
        this.collection = collection.value;
        this.price_current = price_current.value; // required
        this.price_original = price_original.value; // required
        this.colour_brand = colour_brand.value;
        this.colour_hardys = colour_hardys.value; // required
        this.description = description.value;
        this.width = width.value; // required
        this.pattern = pattern.value;
        this.pattern_repeat = pattern_repeat.value;
        this.pattern_match = pattern_match.value;
        this.type = type.value;
        this.retardancy = retardancy.value;
        this.application = application.value;
        this.composition = composition.value;
        this.supplier = supplier.value;
        /* See if checkbox is checked. */
        if (available.checked) {
            this.available = "1";
        } else {
            this.available = "0";
        }
        /* See if any files uploaded. */
        if (image.files.length > 0) {
            this.image = image.files[0];
        } else {
            this.image = null;
        }
    }


    /**
     *
     */
    private getFormValuesInterlining() {
        console.log("Interlining currently not supported!");
    }


    /**
     *
     */
    private getFormValuesLining() {
        console.log("Lining currently not supported!");
    }


    /**
     * Generate a token for CSRF protection via AJAX request to Token.php,
     * then use the token to try registering in via tryRegister().
     */
    private getToken(hasImage: boolean, image_main = null) {
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/Token.php",
            type: "POST",
        }).done(function (token) {
                /* If the image has already been fetched, don't get it again. */
                if (hasImage) {
                    that.tryAddNew(token, image_main);
                } else {
                    that.checkImage(token);
                }
            }
        );
    }


    /**
     *
     * @param token
     */
    private checkImage(token) {
        if (this.image !== null) {
            console.log("Image not null.");
            if (this.image.type.match('image.*')) {
                console.log("File is an image, checking size");
                this.checkImageSize(token);
            } else {
                console.log("File is not an image!");
            }
        } else {
            console.log("Image null, ignoring.");
        }
    }


    /**
     * Check the size of the image.
     */
    private checkImageSize(token) {
        let min_width = 750;
        let min_height = 750;
        let valid_size = false;
        /* Read image */
        let reader = new FileReader();
        let that = this;
        reader.onload = function (e: any) {
            console.log("Reading image...");
            let img = new Image();
            img.src = e.target.result;
            img.onload = function () {
                /* Check image size */
                if (img.width > min_width && img.height > min_height) {
                    valid_size = true;
                    console.log("Reading complete! Image big enough");
                    that.uploadImage(token);
                } else {
                    console.log("Reading complete! Image too small");
                }
            };
        };
        reader.readAsDataURL(this.image);
    }


    /**
     * Upload the image now its fully validated.
     */
    private uploadImage(token) {
        /* Build data package */
        let file_data = new FormData();
        file_data.append('file', this.image);
        file_data.append('token', token);

        console.log("Uploading image...");

        /* Send off request and handle reply. */
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/UploadImage.php",
            data: file_data,
            type: "POST",
            processData: false,
            contentType: false
        }).done(function (reply) {
                let parsed_reply = JSON.parse(reply);
                if (parsed_reply.error) {
                    /* There are errors, list through them. */
                    console.log("Uploading complete! There were errors: ");
                    for (let i = 0; i < parsed_reply.errors.length; i++) {
                        console.log(parsed_reply.errors[i]);
                    }
                } else {
                    /* No errors */
                    console.log("Uploading complete! No errors :D");
                    console.log("Unique ID = " + parsed_reply.uid);
                    /* Get a new token and proceed with rest of form. */
                    that.getToken(true, parsed_reply.uid);
                }
            }
        );
    }


    /**
     * Use the credentials to try registering. Handle any error messages, and
     * provide feedback to the user.
     * @param token
     * @param image_main
     */
    private tryAddNew(token, image_main = null) {

        if (this.product_type === "interlining") {
            this.addNewInterlining(token, image_main);

        } else if(this.product_type === "lining") {
            this.addNewLining(token, image_main);

        } else {
            this.addNewFabric(token, image_main);
        }
    }


    /**
     *
     * @param token
     * @param image_main
     */
    private addNewInterlining(token, image_main = null) {

    }


    /**
     *
     * @param token
     * @param image_main
     */
    private addNewLining(token, image_main = null) {

    }


    /**
     *
     * @param token
     * @param image_main
     */
    private addNewFabric(token, image_main = null) {
        /* Todo: Redundant */
        /*if (image_main === null) {
            console.log("No main image, ignoring");
            image_main = "";
        } else {
            console.log("Main image = " + image_main);
        }*/

        /* Send off request and handle reply. */
        $.ajax({
            url: "/beta/_global/ajax/NewProduct.php",
            data: {
                reference: this.reference,
                title: this.title,
                brand: this.brand,
                collection: this.collection,
                price_current: this.price_current,
                price_original: this.price_original,
                colour_brand: this.colour_brand,
                colour_hardys: this.colour_hardys,
                description: this.description,
                width: this.width,
                pattern: this.pattern,
                pattern_repeat: this.pattern_repeat,
                pattern_match: this.pattern_match,
                type: this.type,
                retardancy: this.retardancy,
                application: this.application,
                composition: this.composition,
                supplier: this.supplier,
                available: this.available,
                /* Todo: processed image name */
                image_main: image_main,
                token: token
            },
            type: "POST",
        }).done(function (reply) {
                let parsed_reply = JSON.parse(reply);
                if (parsed_reply === true) {
                    console.log("New product added successfully!");
                } else if (parsed_reply === false) {
                    console.log("New product ERROR validated OK but failed to insert");
                } else {
                    console.log("New product ERROR validation failed! " +
                        parsed_reply)
                }
            }
        );
    }


    /**
     *
     * @param errors
     */
    private displayErrors(errors) {
        let errors_container = document.createElement('ul');
        errors_container.setAttribute('id', 'errors_container');
        errors_container.innerText = "Please fix the following error(s): -";

        /* Loop through errors and append them. */


        this.formNode.appendChild(errors_container);
    }
}
