<?php
/**
 * new_additional.php
 *
 * @author Robert Hardy
 * @since 2018-09-26
 * Part of the HardysWebSystem project.
 */
?>
<!-- description -->
<div class="field">
    <label for="description">description</label>
    <textarea rows="10" cols="30" title="description"
              id="new-form-description"></textarea>
</div>
<!-- pattern_repeat -->
<div class="field">
    <label for="pattern_repeat">pattern_repeat</label>
    <input type="text" title="pattern_repeat" id="new-form-pattern-repeat">
</div>
<!-- pattern_match -->
<div class="field">
    <label for="pattern_match">pattern_match</label>
    <input type="text" title="pattern_match" id="new-form-pattern-match">
</div>
<!-- reference -->
<div class="field">
    <label for="reference">manufacturer ref</label>
    <input type="text" title="reference" id="new-form-reference">
</div>
<!-- image -->
<div class="field">
    <label for="image">main image</label>
    <input type="file" title="image" id="new-form-image" accept="image/*">
</div>
<!-- retardancy -->
<div class="field">
    <label for="retardancy">retardancy</label>
    <input type="text" title="retardancy" id="new-form-retardancy">
</div>
<!-- composition -->
<div class="field">
    <label for="composition">composition</label>
    <input type="text" title="composition" id="new-form-composition">
</div>
<!-- supplier -->
<div class="field">
    <label for="supplier">supplier</label>
    <input type="text" title="supplier" id="new-form-supplier">
</div>
