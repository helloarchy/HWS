<?php
/**
 * new_reqiured.php
 *
 * @author Robert Hardy
 * @since 2018-09-26
 * Part of the HardysWebSystem project.
 */
?>
<!-- title -->
<div class="field">
    <label for="">title</label>
    <input type="text" title="title" id="new-form-title" required>
</div>
<!-- brand -->
<div class="field">
    <label for="brand">brand</label>
    <input type="text" title="brand" id="new-form-brand" required>
</div>
<!-- collection -->
<div class="field">
    <label for="collection">collection</label>
    <input type="text" title="collection" id="new-form-collection" required>
</div>
<!-- price_current -->
<div class="field">
    <label for="price_current">current price</label>
    <input type="number" title="price_current"
           id="new-form-price-current" required>
</div>
<!-- price_original -->
<div class="field">
    <label for="price_original">original price</label>
    <input type="number" title="price_original"
           id="new-form-price-original" required>
</div>
<!-- width -->
<div class="field">
    <label for="width">width</label>
    <input type="number" title="width" id="new-form-width" required>
</div>
<!-- colour_brand -->
<div class="field">
    <label for="colour_brand">brand colour</label>
    <input type="text" title="colour_brand" id="new-form-colour-brand">
</div>
<!-- colour_hardys -->
<div class="field">
    <label for="colour_hardys">hardy's colour</label>
    <select title="colour_hardys" size="5"
            id="new-form-colour-hardys" required>
        <option value="red">red</option>
        <option value="orange">orange</option>
        <option value="yellow">yellow</option>
        <option value="green">green</option>
        <option value="blue">blue</option>
        <option value="violet">violet</option>
        <option value="black">black</option>
        <option value="grey">grey</option>
        <option value="white">white</option>
        <option value="brown">brown</option>
    </select>
</div>
<!-- pattern -->
<div class="field">
    <label for="pattern">pattern</label>
    <select title="pattern" size="5" id="new-form-pattern" required>
        <option value="floral">floral</option>
        <option value="geometric">geometric</option>
        <option value="stripe">stripe</option>
        <option value="damask">damask</option>
        <option value="check">check</option>
        <option value="plain">plain</option>
    </select>
</div>
<!-- type -->
<div class="field">
    <label for="type">type</label>
    <select title="type" size="5" id="new-form-type" required>
        <option value="embroidered">embroidered</option>
        <option value="plain">plain</option>
        <option value="print">print</option>
        <option value="sheer/voile">sheer/voile</option>
        <option value="silk">silk</option>
        <option value="suede/faux">suede/faux</option>
        <option value="velvet">velvet</option>
        <option value="vinyl">vinyl</option>
        <option value="weave">weave</option>
        <option value="wooden">wooden</option>
        <option value="wool">wool</option>
    </select>
</div>
<!-- application(s) -->
<div class="field">
    <label for="application" style="text-transform: none">Application(s)</label>
    <select title="application" size="5" id="new-form-application" multiple required>
        <option value="curtains">curtains</option>
        <option value="blinds">blinds</option>
        <option value="cushions">cushions</option>
        <option value="upholstery">upholstery</option>
        <option value="headboards">headboards</option>
        <option value="universal">universal</option>
    </select>
</div>
<!-- available -->
<div class="field">
    <label for="available">available</label>
    <input type="checkbox" title="available" id="new-form-available">
</div>
