<?php
/**
 * Add New Product - new.php
 * Part of the Admin Products page.
 * @author Robert Hardy
 * @since 2018-09-10
 * Part of the Hardy'sWebSystem project.
 */
?>
<!-- Section Head -->
<h3 class="border" id="form-new-title">
    add a new product
</h3>
<!-- Choose product type -->
<div class="product-new-section">
    <h3>
        type
    </h3>
    <div id="choose-type-container">
        <button class="type-choice"
                onclick="window.location = 'index.php?action=new&type=fabric';">
            fabric
        </button>
        <button class="type-choice"
                onclick="window.location = 'index.php?action=new&type=interlining';">
            interlining
        </button>
        <button class="type-choice"
                onclick="window.location = 'index.php?action=new&type=lining';">
            lining
        </button>
    </div>
</div>
<form id="product-new-container">
    <?php
    /* Set the locations of the includes file depending on the product type,
    if none set then default to Fabric.*/
    $required = "fields/required_fabric.php";
    $additional = "fields/additional_fabric.php";
    if (isset($_GET['type'])) {
        $type = $_GET['type'];
        if ($type === "interlining") {
            /* Interlining fields */
            $required = "fields/required_interlining.php";
            $additional = "fields/additional_interlining.php";
        } else if ($type === "lining") {
            /* Lining fields */
            $required = "fields/required_lining.php";
            $additional = "fields/additional_lining.php";
        }
    }
    ?>

    <!-- Required fields -->
    <div class="product-new-section" id="fields-required-container">
        <h3>
            required
        </h3>
        <?php include $required; ?>
    </div>

    <!-- Not required fields -->
    <div class="product-new-section" id="fields-required-container">
        <h3>
            additional
        </h3>
        <?php include $additional; ?>
    </div>
    <!-- submit -->
    <input type="submit" id="form-new-submit">
</form>

