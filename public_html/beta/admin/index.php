<?php

/**
 * Admin - index.php
 * This file defines the admin page, where an admin can manage the web system.
 * @author Robert Hardy
 * @since 2018-09-07
 * Part of the Hardy'sWebSystem project.
 */

/* Initialise */
require_once __DIR__ . '/../../../application/core/initialise.php';

/* Make sure user is an admin, if not, redirect. */
$user = new User();
if ($user->hasPermission(2)) {
/* User has required access level, produce page... */
/* Check GET for sign out action */
if (Input::exists('get')) {
    if (Input::get('action') === 'sign out') {
        $user->logout();
        Redirect::to("/beta/account/index.php");
        ?>
        <p style="color: red; font-size: large">
            You do not have permission to view this page.
        </p>
        <?php
    } else if (Input::get('action') === 'products') {
        Redirect::to("products/index.php");
    }
}
?>
<!DOCTYPE HTML>
<html lang="en">
<head>
    <title>Jones &amp Hicks - Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- JavaScript Files -->
    <script src="../_global/script/class/NavSite.js"></script>
    <script src="../_global/script/class/NavAccount.js"></script>
    <script src="../_global/script/class/NavBasket.js"></script>
    <script src="../_global/script/class/NavFooter.js"></script>

    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-site.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-account.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-side.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/header.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/default.css">
    <link rel="stylesheet" type="text/css" href="_style/_main.css">
    <link rel="stylesheet" type="text/css" href="_style/stats.css">

</head>
<body>
<!-- Layout the grid -->
<div id="grid-page">

    <!-- Header -->
    <header class="border grid-box" id="header">
        <div class="border" id="header-title-account-basket-row">
            <h1 id="header-title">
                Jones &amp Hicks
            </h1>
            <div class="border inner-box" id="nav-account-container">
                Account
            </div>
            <div class="border inner-box" id="nav-basket-container">
                Basket
            </div>
        </div>
        <!-- Site Nav Bar -->
        <div class="border" id="header-nav-site-row">
            <nav class="border inner-box" id="nav-site-container">
                <!-- Populate by script -->
            </nav>
            <div class="border inner-box spacer-nav">
                <i>Spacer</i>
            </div>
        </div>
    </header>

    <!-- Sidebar Left -->
    <div class="border grid-box" id="sidebar-left">
        Administration
        <!-- Actions Panel -->
        <div class="border" id="profile-actions-container">
            <!-- Populated via script -->
        </div>
    </div>

    <!-- Product List -->
    <main class="border grid-box" id="main">
        Quick Stats:
        <div class="border" id="stats-container">
            <!-- Orders -->
            <div class="border stats-box" id="stats-orders">
                <h3 class="text-head stats-box-header">
                    Orders
                </h3>
                <div class="stats-itemised-container">
                    To be added via script...
                </div>
            </div>
            <!-- Users -->
            <div class="border stats-box" id="admin-stats-users">
                <h3 class="text-head stats-box-header">
                    Users
                </h3>
                <div class="stats-itemised-container">
                    To be added via script...
                </div>
            </div>
            <!-- Products -->
            <div class="border stats-box" id="admin-stats-products">
                <h3 class="text-head stats-box-header">
                    Products
                </h3>
                <div class="stats-itemised-container">
                    To be added via script...
                </div>
            </div>
        </div>
    </main>

    <!-- Footer -->
    <footer class="border grid-box" id="footer">
        Footer
        <div class="border inner-box" id="nav-footer-container">
            Footer nav
        </div>
    </footer>

    <!-- Scripts for handling the above -->
    <script>
        /* Header Navigation */
        let navSite = new NavSite("nav-site-container");
        let navAccount = new NavAccount("nav-account-container");
        let navBasket = new NavBasket("nav-basket-container");

        /* Populate the side bar with the account nav actions */
        navAccount.createList(<?= $user->getIsLoggedIn(); ?>,
            <?= $user->getPermission(); ?>, "profile-actions-container");

        /* Footer Navigation */
        let footer = new NavFooter("nav-footer-container");
    </script>
</div>
</body>
<?php
} else {
    /* user does not have at least level 2 */
    Redirect::to(404);
}