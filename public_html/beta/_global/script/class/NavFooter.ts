/**
 * NavFooter - NavFooter.ts.ts
 * This file defines the footer for the site, which includes many useful links
 * and information related to the company.
 *
 * @author Robert Hardy
 * @since 2018-08-07
 * Part of the Hardy'sWebSystem project.
 */
class NavFooter {
    private parentID: string;

    constructor(parentID: string) {
        this.parentID = parentID;
    }
}