/**
 * Navigation for Basket - NavBasket.ts
 * This file defines the basket featured within the navigation bar for use on
 * each web page, to be reused across all web pages where applicable.
 *
 * @author Robert Hardy
 * @since 2018-08-07
 * Part of the Hardy'sWebSystem project.
 */
class NavBasket {
    private parentID: string;

    constructor(parentID: string) {
        this.parentID = parentID;
    }
}