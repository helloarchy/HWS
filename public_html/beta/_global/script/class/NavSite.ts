/**
 * Site-wide Navigation Bar - NavSite.ts
 * This file defines a navigation bar for use on each web page, to be reused
 * across all web pages where applicable without having to rewrite code.
 *
 * @author Robert Hardy
 * @since 2018-08-02
 * Part of the Hardy'sWebSystem project.
 */
class NavSite {
    private readonly parentNode: HTMLElement;
    private readonly pages: string[][];

    /**
     * Create a new site navigation
     * @param parentID
     */
    constructor(parentID: string) {
        this.parentNode = document.getElementById(parentID);
        this.pages = [
            ["/beta/index.php", "home"],
            ["/beta/explore/index.php", "explore"]
        ];

        /* Create html for each nav link */
        for (let i = 0; i < this.pages.length; i++) {
            this.createHTML(this.pages[i][0], this.pages[i][1]);
        }
    }


    /**
     * Create the HTML for each navigation element
     * @param page_url
     * @param page_title
     */
    private createHTML(page_url: string, page_title: string) {
        /* Create child node */
        let container = document.createElement('div');
        container.setAttribute('class', 'nav-site-link-container');

        /* Create link */
        let link = document.createElement('a');
        link.setAttribute('href', page_url);

        /* Create text node */
        let text = document.createTextNode(page_title);

        /* Append all */
        link.appendChild(text);
        container.appendChild(link);
        this.parentNode.appendChild(container);
    }
}