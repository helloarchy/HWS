/**
 * Navigation for Account - NavAccount.ts
 * This file defines the navigation for the user account featured within the
 * navigation bar for use on each web page, to be reused across all web pages
 * where applicable.
 *
 * @author Robert Hardy
 * @since 2018-08-31
 * Part of the Hardy'sWebSystem project.
 */
class NavAccount {
    private parentNode: HTMLElement;
    private readonly navActionsSignedIn: string[][];
    private readonly navActionsSignedOut: string[][];
    private readonly navActionsAdmin: string[][];

    constructor(parentID: string) {
        this.parentNode = document.getElementById(parentID);
        this.navActionsSignedIn = [
            /* [Title, action] */
            ["orders", "orders"],
            ["sign out", "sign out"]
        ];
        this.navActionsSignedOut = [
            /* [Title, action] */
            ["sign in", "sign in"],
            ["register", "register"]
        ];
        this.navActionsAdmin = [
            /* Title, action */
            ["orders", "orders"],
            ["users", "users"],
            ["products", "products"],
            ["sign out", "sign out"]
        ];

        /* See if user already logged in, if so provide drop down options, else
         * create link to account page for logging in or registering. */
        this.getUserStatus();
    }

    /**
     *
     */
    private getUserStatus() {
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/Account.php",
            data: {
                query: "getUserStatus"
            },
            type: "POST",
        }).done(function (reply) {
                let parsedReply = JSON.parse(reply);
                console.log("NavAccount: ajax reply = " +
                    "logged in: " + parsedReply[0] +
                    ", level: " + parsedReply[1]);
                that.createHTML(parsedReply[0], parsedReply[1]);
            }
        );
    }


    /**
     * See if user is logged in, and create the relevant account button and list.
     * @param isLoggedIn
     * @param userLevel
     */
    private createHTML(isLoggedIn: boolean, userLevel: number) {
        this.clearHTML();
        /* Create the button */
        let button = document.createElement('button');
        button.setAttribute('id', 'nav-account-button');
        button.setAttribute('class', 'header-nav-icon');

        /* See if user is an admin and set button text and
        href location accordingly */
        let href = "";
        if (isLoggedIn && userLevel >= 2) {
            /* User is an admin */
            button.innerText = "Admin \u25BC";
            href = "/beta/admin/index.php";
        } else {
            button.innerText = "Account \u25BC";
            href = "/beta/account/index.php";
        }

        /* Create the list */
        let list = this.createList(isLoggedIn, userLevel);

        /* Append everything */
        this.parentNode.appendChild(button);
        this.parentNode.appendChild(list);

        /* Add event listener */
        let that = this;
        let button_elem = document.getElementById('nav-account-button');
        button_elem.addEventListener('click', function () {
            console.log("Account button clicked.");
            window.location.href = href;
        });
    }


    /**
     *
     * @param signed_in
     * @param user_level
     * @param parentID
     * @return list, else null.
     */
    public createList(signed_in: boolean, user_level: number,
                      parentID: string = null) {
        /* Get the list of nav actions depending on if the user is
         * signed in or not.  */
        let nav_actions = [];
        let href = "/beta/account/index.php?action=";
        if (signed_in) {
            if (user_level >= 2) {
                /* User is an admin */
                nav_actions = this.navActionsAdmin;
                href = "/beta/admin/index.php?action=";
            } else {
                /* User is a regular member */
                nav_actions = this.navActionsSignedIn;
            }
        } else {
            /* User is a guest */
            nav_actions = this.navActionsSignedOut;
        }

        /* Create the list container */
        let list = document.createElement('div');

        /* Loop through all the possible user actions, creating a list item
        * for each and appending it to the list. */
        for (let action of nav_actions) {
            let list_item = document.createElement('a');
            list_item.innerText = action[0];
            list_item.href = href + action[1];
            list.appendChild(list_item);
        }

        /* If a parent is given, append it to the parent, otherwise return
        * the list as it is. */
        if (parentID == null) {
            /* Set the list id to drop down */
            list.setAttribute('id', 'nav-account-list-drop');
            return list;
        } else {
            /* Set the list id to static panel and append it */
            list.setAttribute('id', 'nav-account-list-static');
            list.setAttribute('class', 'nav-side');
            let parent = document.getElementById(parentID);
            parent.appendChild(list);
            return null;
        }
    }


    /**
     * Clear the Account of all html.
     */
    private clearHTML() {
        this.parentNode.innerHTML = "";
    }
}