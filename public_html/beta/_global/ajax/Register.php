<?php
/**
 * Registration - register.php
 * @author Robert Hardy
 * @since 2018-09-02
 *
 * This file defines the registration form page for the site.
 */
require_once __DIR__ . '/../../../../application/core/initialise.php';

if (Input::exists()) {
    if (Token::check(Input::get('token'))) { // Prevent anything being passed via URL
        $validate = new Validate();
        $validate->check($_POST, array(
            // Set of rules for validation and requirements, etc
            'email' => array(
                // Rules for email
                'required' => true,
                'min' => 6, // goo.gl/qUat5B
                'max' => 255,
                'unique' => 'user' // Unique in the users table
            ),
            'email_confirm' => array(
                // Rules for email
                'required' => true,
                'min' => 6, // goo.gl/qUat5B
                'max' => 255,
                'matches' => 'email'
            ),
            'password' => array(
                // Rules for password
                'required' => true,
                'min' => 6 // TODO: method for ensuring strong password?
            ),
            'password_confirm' => array(
                // Rules for password confirmation
                'required' => true,
                'matches' => 'password'
            )
        ));


        // Check if passes validation
        if ($validate->getPassed()) {
            $user = new User();
            try {
                $user->create(array(
                    'email' => Input::get('email'),
                    'password' => Hash::make(Input::get('password'))
                ));
                echo "Registration successful";
            } catch (Exception $e) {
                die($e->getMessage());
                // TODO: Handle error on registration with feedback informative
            }
        } else {
            // Output errors
            foreach ($validate->getErrors() as $error) {
                echo json_encode($error . ", ");
            }
        }
    } else {
        echo "token check fail, current token = " .
            Session::get(Config::get('session/token_name'));
    }
} else {
    echo "No input!";
}