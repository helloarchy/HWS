<?php
/**
 * Login.php
 *
 * @author Robert Hardy
 * @since 2018-09-02
 * Part of the Hardy'sWebSystem project.
 */
require_once __DIR__ . '/../../../../application/core/initialise.php';

// Check if any input exists
if (Input::exists()) {
    if (Token::check(Input::get('token'))) {
        // Check validation
        $validate = new Validate();
        $validate->check($_POST, array(
            'email' => array('required' => true),
            'password' => array('required' => true)
        ));

        // See if passes validation
        if ($validate->getPassed()) {
            $user = new User();
            $remember = (Input::get('remember') === 'on') ? true : false;
            // Log user in
            $login = $user->login(Input::get('email'),
                Input::get('password'), $remember);

            if ($login) {
                // Success, redirect home
                echo json_encode(true);
            } else {
                echo json_encode(false);
            }
        } else {
            foreach ($validate->getErrors() as $error) {
                echo json_encode($error, '</br>');
            }
        }
    } else {
        echo "token check fail, current token = " .
        Session::get(Config::get('session/token_name'));
    }
} else {
    echo "No input!";
}
