<?php
/**
 * ProductList.php
 * This file provides a list of products which meet filter and sort requirements.
 *
 * @author Robert Hardy
 * @since 2018-08-16
 * Part of the Hardy'sWebSystem project.
 */

// Initialise everything from init.
require_once __DIR__ . '/../../../../application/core/initialise.php';
$product_handler = new ProductHandler();

/* Query the AJAX request */
if (isset($_POST['N'])) {
    /* See if we need the first batch of results or the next lot */
    if (isset($_POST['batch'])) {
        if ($_POST['batch'] == 'first') {
            /* Check if filtered results are required (if not, usually initial) */
            if (isset($_POST['filtered_sorted'])) {
                if ($_POST['filtered_sorted'] == "false") {
                    /* Get first lot of unfiltered results */
                    $results = $product_handler->getFirst($_POST['N']);
                    echo $results;
                } else if ($_POST['filtered_sorted'] == "true") {
                    echo $product_handler->getFirst(
                        $_POST['N'],
                        $_POST['brand'],
                        $_POST['collection'],
                        $_POST['price_current'],
                        $_POST['colour_hardys'],
                        $_POST['width'],
                        $_POST['pattern'],
                        $_POST['type'],
                        $_POST['application'],
                        $_POST['sort']
                    );
                } else {
                    echo "filter invalid";
                }
            }
        } else if ($_POST['batch'] == 'next') {
            /* Get the next N lot */
            echo $product_handler->getNext($_POST['N']);
        }
    } else if (isset($_POST['hasNext'])) {
        /* See if there is a next lot to get */
        if ($_POST['hasNext'] == "true") {
            echo $product_handler->hasNext($_POST['N']);
        }
    }
}