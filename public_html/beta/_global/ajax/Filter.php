<?php
/**
 * Filter.php
 * This file defines the ability to filter the product lists by certain parameters,
 * while also providing a range of applicable filters to ProductFilter.ts for
 * displaying to the user.
 *
 * @author Robert Hardy
 * @since 2018-08-02
 * Part of the Hardy'sWebSystem project.
 */

// Initialise everything from init.
require_once __DIR__ . '/../../../../application/core/initialise.php';

/* Query the AJAX request */
if (isset($_POST['getAllFilters'])) {
    if ($_POST['getAllFilters'] == "true") {
        getAllFilters();
    }
}


/**
 *
 */
function getAllFilters()
{
    $all_results = array(
        "columns" => array(
            "brands" => queryDB("brand"),
            "collections" => queryDB("collection"),
            "current_prices" => queryDB("price_current"),
            "colours" => queryDB("colour_hardys"),
            "widths" => queryDB("width"),
            "patterns" => queryDB("pattern"),
            "types" => queryDB("type"),
            "applications" => queryDB("application")
        )
    );
    echo json_encode($all_results);
}


/**
 * @param $column
 * @return mixed
 */
function queryDB($column)
{
    $db = Database::getInstance();
    $sql = "select distinct {$column} from product_fabric where available = 1";
    $results = $db->getColumn($sql);
    return $results->results();
}
