<?php
/**
 * UploadImage.php
 *
 * @author Robert Hardy
 * @since 2018-09-13
 * Part of the HardysWebSystem project.
 */
require_once __DIR__ . '/../../../../application/core/initialise.php';

$reply = array("error" => false, "errors" => array());

/* Check for input, then check token is valid */
if (Input::exists()) {
    if (Token::check(Input::get('token'))) {
        /* Token OK, trying to move image */
        if (0 < $_FILES['file']['error']) {
            $reply['error'] = true;
            array_push($reply['errors'], $_FILES['file']['error']);
            echo json_encode($reply);
        } else {
            $image_source = __DIR__ . '/../../_upload/' .
                $_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'], $image_source);

            /* Make the resized images from the image. */
            makeResizedImages($reply, $image_source, $_FILES['file']['type']);
        }
    } else {
        echo json_encode("token check fail, current token = " .
            Session::get(Config::get('session/token_name')));
    }
} else {
    $reply['error'] = true;
    array_push($reply['errors'], "no input!");
    echo json_encode($reply);
}


/**
 * Handle the creation of the various different image sizes from the
 * source image, and handle errors if any occur.
 * @param $reply
 * @param $filename
 * @param $type
 */
function makeResizedImages($reply, $filename, $type) {
    /* Get image details */
    list($width, $height, $type) = getimagesize($filename);

    /* Create image from type */
    switch ($type) {
        case IMAGETYPE_JPEG:
            $image = imagecreatefromjpeg($filename);
            break;
        case IMAGETYPE_PNG:
            $image = imagecreatefrompng($filename);
            break;
        case IMAGETYPE_GIF:
            $image = imagecreatefromgif($filename);
            break;
        default:
            $reply['error'] = true;
            array_push($reply['errors'],
                "file type {$type} not supported!");
            echo json_encode($reply);
            exit();
    }

    /* Resize to required sizes, using current images details.  */
    $image_xs = resizeByWidth(100, $image, $width, $height);
    $image_s = resizeByWidth(250, $image, $width, $height);
    $image_m = resizeByWidth(500, $image, $width, $height);
    $image_l = resizeByWidth(750, $image, $width, $height);
    $image_xl = resizeByWidth(1000, $image, $width, $height);

    /* Save images to relevant folder using a unique ID as filename */
    $uid = uniqid();
    $location = __DIR__ . "/../../_assets/image/product";
    while (file_exists($location . "/xs/{$uid}.jpg")) {
        /* Generate a new UID if a file already exists with that name. */
        $uid = uniqid();
    }
    imagejpeg($image_xs, $location . "/xs/{$uid}.jpg",100);
    imagejpeg($image_s, $location . "/s/{$uid}.jpg",100);
    imagejpeg($image_m, $location . "/m/{$uid}.jpg",100);
    imagejpeg($image_l, $location . "/l/{$uid}.jpg",100);
    imagejpeg($image_xl, $location . "/xl/{$uid}.jpg", 100);

    /* Reply success */
    $reply['error'] = false;
    $reply['uid'] = "{$uid}.jpg";
    echo json_encode($reply);
}


/**
 * Resize an image to a given width, preserving aspect ratio.
 * @param $width_new
 * @param $image
 * @param $width_old
 * @param $height_old
 * @return resource
 */
function resizeByWidth($width_new, $image, $width_old, $height_old) {
    $ratio = $width_new / $width_old;
    $height_new = $height_old * $ratio;
    $new_image = imagecreatetruecolor($width_new, $height_new);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0,
        $width_new, $height_new, $width_old, $height_old);
    return $new_image;
}