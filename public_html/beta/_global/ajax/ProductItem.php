<?php
/**
 * ProductItem.php
 * Handle AJAX requests for an individual product item page.
 * @author Robert Hardy
 * @since 2018-08-22
 * Part of the Hardy'sWebSystem project.
 */

// Initialise everything from init.
require_once __DIR__ . '/../../../../application/core/initialise.php';

if (isset($_POST['colourways'])) {
    if ($_POST['colourways'] == "true") {
        if (isset($_POST['title'])) {
            /* Decode the title now we know its set */
            $title = json_decode($_POST['title']);
            if (isset($_POST['brand'])) {
                /* Decode the brand now we know its set */
                $brand = json_decode($_POST['brand']);
                if (isset($_POST['collection'])) {
                    /* Decode the collection now we know its set */
                    $collection = json_decode($_POST['collection']);
                    if (isset($_POST['colour'])) {
                        /* Decode the colour now we know its set */
                        $colour = json_decode($_POST['colour']);

                        /* Get the results for matching products but
                        with different colours */
                        echo getColourways($title, $brand, $collection, $colour);
                    } else {
                        echo "colour not set!";
                    }
                } else {
                    echo "collection not set!";
                }
            } else {
                echo "brand not set!";
            }
        } else {
            echo "title not set!";
        }
    } else {
        echo "colourways is set, but is not true";
    }
} else {
    echo "colourways not set";
}

/**
 *
 * @param $title
 * @param $brand
 * @param $collection
 * @param $colour
 * @return null
 */
function getColourways($title, $brand, $collection, $colour)
{
    /* Query database */
    $db = Database::getInstance();
    $sql = "select ufid, colour_brand, image_main
            from product_fabric where 
            title = '{$title}' and
            brand = '{$brand}' and
            collection = '{$collection}' and
            not colour_brand = '{$colour}';";
    $results = $db->query($sql);

    /* Convert to array and return */
    $results_as_array = array();
    foreach ($results->results() as $row) {
        $results_as_array[] = $row;
    }
    return json_encode($results_as_array);
}
