<?php
/**
 * Account.php
 * Handle all AJAX requests related to the users account.
 * @author Robert Hardy
 * @since 2018-08-31
 * Part of the Hardy'sWebSystem project.
 */

// Initialise everything from init.
require_once __DIR__ . '/../../../../application/core/initialise.php';

if (isset($_POST['query'])) {
    if ($_POST['query'] == "getUserStatus") {
        $user = new User();
        $reply = array();
        if ($user->getIsLoggedIn()) {
            $reply = array(true, $user->getPermission());
        } else {
            $reply = array(false, 0);
        }
        echo json_encode($reply);
    }
}