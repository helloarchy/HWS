<?php
/**
 * Token.php
 * For requesting the token on pages which contain more than one form. To prevent
 * Token.php from overriding another forms token.
 * @author Robert Hardy
 * @since 2018-09-02
 * Part of the Hardy'sWebSystem project.
 */
require_once __DIR__ . '/../../../../application/core/initialise.php';
echo Token::generate();