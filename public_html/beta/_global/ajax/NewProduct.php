<?php
/**
 * NewProduct.php
 *
 * @author Robert Hardy
 * @since 2018-09-10
 * Part of the Hardy'sWebSystem project.
 */
require_once __DIR__ . '/../../../../application/core/initialise.php';

if (Input::exists()) {
    if (Token::check(Input::get('token'))) { // Prevent anything being passed via URL
        $validate = new Validate();
        $validate->check($_POST, array(
            // Set of rules for validation and requirements, etc
            'title' => array(
                // Rules for email
                'required' => true,
                'min' => 0, // goo.gl/qUat5B
                'max' => 255,
            )
        ));

        // Check if passes validation
        if ($validate->getPassed()) {

            /* Check through POST form data and exclude empty values. Get the
            index names for the related database columns to be used with insert. */
            $fields = array();
            foreach ($_POST as $name => $value) {
                /* Only add field if value is not blank */
                if (!($value === "")) {
                    /* And field is not the token or image. */
                    if (!($name === "token")) {
                        $fields += array($name => $value);
                    }
                }
            }

            /* Try inserting product into database. */
            $db = Database::getInstance();
            $success = $db->insert('product_fabric', $fields);

            /* If successful, return true, else return the errors. */
            echo json_encode($success);
        } else {
            // Output errors
            foreach ($validate->getErrors() as $error) {
                echo json_encode($error . ", ");
            }
        }
    } else {
        echo "token check fail, current token = " .
            Session::get(Config::get('session/token_name'));
    }
} else {
    echo "No input!";
}