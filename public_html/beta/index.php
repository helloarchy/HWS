<!DOCTYPE HTML>
<html lang="en">
<head>
    <!--
    Index, Home/Landing page - index.php
    This file defines the home page for the site.
    @author Robert Hardy
    @since 2018-06-15
    Part of the Hardy'sWebSystem project.
    -->
    <title>Jones &amp Hicks - Home</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>

    <!-- JavaScript Files -->
    <script src="_global/script/class/NavSite.js"></script>
    <script src="_global/script/class/NavAccount.js"></script>
    <script src="_global/script/class/NavBasket.js"></script>
    <script src="_global/script/class/NavFooter.js"></script>

    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="_global/style/default.css">
    <link rel="stylesheet" type="text/css" href="_global/style/nav-site.css">
    <link rel="stylesheet" type="text/css" href="_global/style/header.css">
    <link rel="stylesheet" type="text/css" href="_global/style/nav-account.css">
    <link rel="stylesheet" type="text/css" href="home/style/_main.css">

    <?php
    // Initialise everything from init.
    require_once __DIR__ . '/../../application/core/initialise.php';
    ?>
</head>
<body>
<!-- Layout the grid -->
<div id="grid-page">

    <!-- Header -->
    <header class="border grid-box" id="header">
        <div class="border" id="header-title-account-basket-row">
            <h1 id="header-title">
                Jones &amp Hicks
            </h1>
            <div class="border inner-box" id="nav-account-container">
                Account
            </div>
            <div class="border inner-box" id="nav-basket-container">
                Basket
            </div>
        </div>
        <!-- Site Nav Bar -->
        <div class="border" id="header-nav-site-row">
            <nav class="border inner-box" id="nav-site-container">
                <!-- Populate by script -->
            </nav>
            <div class="border inner-box spacer-nav">
                <i>Spacer</i>
            </div>
        </div>
    </header>

    <!-- Sidebar Left -->
    <div class="border grid-box" id="sidebar-left">
        Sidebar (Left)
    </div>

    <!-- Product List -->
    <main class="border grid-box" id="main">

    </main>


    <!-- Footer -->
    <footer class="border grid-box" id="footer">
        Footer
        <div class="border inner-box" id="nav-footer-container">
            Footer nav
        </div>
    </footer>

    <!-- Scripts for handling the above -->
    <script>
        /* Header Navigation */
        let navSite = new NavSite("nav-site-container");
        let navAccount = new NavAccount("nav-account-container");
        let navBasket = new NavBasket("nav-basket-container");

        /* Footer Navigation */
        let footer = new NavFooter("nav-footer-container");
    </script>
</div>
</body>
</html>