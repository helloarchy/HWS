<!DOCTYPE HTML>
<html lang="en">
<head>
    <!--
    product/index.php
    This page gives a detailed view of an individual product.
    @author Robert Hardy
    @since 2018-08-21
    Part of the Hardy'sWebSystem project.
    -->
    <title>Hardy's - Explore</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- JavaScript Files -->
    <script src="../_global/script/class/NavSite.js"></script>
    <script src="../_global/script/class/NavAccount.js"></script>
    <script src="../_global/script/class/NavBasket.js"></script>
    <script src="../_global/script/class/NavFooter.js"></script>
    <script src="script/Colourway.js"></script>
    <script src="script/Magnify.js"></script>

    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-site.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/header.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/default.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-account.css">
    <link rel="stylesheet" type="text/css" href="style/_main.css">
    <link rel="stylesheet" type="text/css" href="style/product.css">
    <link rel="stylesheet" type="text/css" href="style/gallery.css">
    <link rel="stylesheet" type="text/css" href="style/info_main.css">
    <link rel="stylesheet" type="text/css" href="style/colour.css">
    <link rel="stylesheet" type="text/css" href="style/info_additional.css">

    <!-- Set up product references. -->
    <?php
    /* Initialise everything from init. */
    require_once __DIR__ . '/../../../application/core/initialise.php';

    /* Query database using GET value */
    $db = Database::getInstance();
    $sql = "select * from product_fabric where ufid = " . hexdec($_GET['ufid']) . ";";
    $results = $db->query($sql);

    /* Assign results if any, otherwise 404 */
    if (!($results->count())) {
        Redirect::to(404);
    } else {
        $product = $results->first();
    }
    ?>
</head>
<body>
<!-- Layout the grid -->
<div class="grid-page">

    <!-- Header -->
    <header class="border grid-box" id="header">
        <div class="border" id="header-title-account-basket-row">
            <h1 id="header-title">
                Jones &amp Hicks
            </h1>
            <div class="border inner-box" id="nav-account-container">
                Account
            </div>
            <div class="border inner-box" id="nav-basket-container">
                Basket
            </div>
        </div>
        <!-- Site Nav Bar -->
        <div class="border" id="header-nav-site-row">
            <nav class="border inner-box" id="nav-site-container">
                <!-- Populate by script -->
            </nav>
            <div class="border inner-box spacer-nav">
                <i>Spacer</i>
            </div>
        </div>
    </header>

    <!-- Main Product Container -->
    <main class="box border main product">

        <!-- Gallery -->
        <div class="border" id="gallery-container">
            Gallery

            <!-- Big image Container -->
            <div class="border" id="image-big-container">
                <div id="image-big" style="background-image: url(
                <?php
                if (isset($product->image_main)) {
                    echo "../_assets/image/product/l/"
                        . $product->image_main;
                } else {
                    echo "../_assets/image/error_product_default.png";
                }
                ?>);">
                </div>
                <div id="magnify-result" class="magnify-result">
                    <!-- Don't bother because no mobile support, click zoom -->
                </div>
            </div>

            <!-- Thumbnails Container -->
            <div id="image-thumbs-container">
                Thumbnail container
            </div>
        </div>

        <!-- Main information -->
        <div class="border" id="info-main-container">

            <!-- Title etc. -->
            <div class="border" id="title-brand-collection-container">
                <p class="border text-head" id="product-title">
                    <?= $product->title; ?>
                </p>
                <p class="border text-head" id="product-brand">
                    <?= $product->brand; ?>
                </p>
                <p class="border text-head" id="product-collection">
                    <?= $product->collection; ?>
                </p>
            </div>

            <!-- Price(s) -->
            <div class="border" id="price-container">
                <p class="text-head" id="price_current">
                    <?= "£" . $product->price_current . " / metre" ?>
                </p>
            </div>

            <!-- Description -->
            <div class="border" id="description-container">
                <p class="text-body" id="description">
                    <?= $product->description; ?>
                </p>
            </div>

            <!-- Colour and Colourways -->
            <div class="border" id="colours-container">
                <div class="text-head" id="colour">
                    <?= "Colour - " . $product->colour_hardys; ?>
                </div>
                <div id="colourways-container">
                    Colourways: <br>
                    ... Colourways via script ...
                </div>
            </div>

            <!-- Buttons etc. -->
            <div class="border" id="action-buttons-form-container">
                Add to basket etc.
            </div>
        </div>

        <!-- Additional Information -->
        <div class="border" id="additional-info-container">
            <div class="additional-info-item-container">
                <p class="additional-info-head">
                    Width:
                </p>
                <p class="additional-info-body" style="text-transform: none">
                    <?= $product->width . " cm" ?>
                </p>
            </div>
            <div class="additional-info-item-container">
                <p class="additional-info-head">
                    Pattern:
                </p>
                <p class="additional-info-body">
                    <?= $product->pattern; ?>
                </p>
            </div>
            <div class="additional-info-item-container">
                <p class="additional-info-head">
                    Pattern Repeat:
                </p>
                <p class="additional-info-body" style="text-transform: none">
                    <?= $product->pattern_repeat . " cm" ?>
                </p>
            </div>
            <div class="additional-info-item-container">
                <p class="additional-info-head">
                    Pattern Match:
                </p>
                <p class="additional-info-body">
                    <?= $product->pattern_match; ?>
                </p>
            </div>
            <div class="additional-info-item-container">
                <p class="additional-info-head">
                    Type:
                </p>
                <p class="additional-info-body">
                    <?= $product->type; ?>
                </p>
            </div>
            <div class="additional-info-item-container">
                <p class="additional-info-head">
                    Application:
                </p>
                <p class="additional-info-body">
                    <?= $product->application; ?>
                </p>
            </div>
        </div>
    </main>

    <!-- Basket sidebar? -->
    <div class="box border sidebar">
        Order Progress?
    </div>

    <!-- Footer -->
    <footer class="border grid-box" id="footer">
        Footer
        <div class="border inner-box" id="nav-footer-container">
            Footer nav
        </div>
    </footer>

    <!-- Scripts for handling the above -->
    <script>
        /* Header Navigation */
        let navSite = new NavSite("nav-site-container");
        let navAccount = new NavAccount("nav-account-container");
        let navBasket = new NavBasket("nav-basket-container");

        /* TODO: Other dynamic generation here... */
        let colourways = new Colourway("colourways-container",
            "<?=$product->title;?>",
            "<?=$product->brand;?>",
            "<?=$product->collection;?>",
            "<?=$product->colour_hardys;?>"
        );

        /*magnify("image-big", "magnify-result");*/

        /* Footer Navigation */
        let footer = new NavFooter("nav-footer-container");
    </script>
</div>
</body>
