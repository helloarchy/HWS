/**
 * Colourways - Colourway.ts
 * This file defines the creation of various colourway icons and event handling
 * for a particular products optional other colours.
 *
 * @author Robert Hardy
 * @since 2018-08-22
 * Part of the Hardy'sWebSystem project.
 */
class Colourway {
    private parentNode: HTMLElement;
    private title: string;
    private brand: string;
    private collection: string;
    private colour: string;


    /**
     *
     * @param parentID
     * @param title
     * @param brand
     * @param collection
     * @param colour
     */
    constructor(parentID: string, title: string, brand: string,
                collection: string, colour: string) {
        this.parentNode = document.getElementById(parentID);
        this.title = title;
        this.brand = brand;
        this.collection = collection;
        this.colour = colour;

        /* Get colour ways and create HTML and event handlers for them */
        this.getColourways();
    }


    /**
     * Get the colourways for this product item via AJAX.
     */
    private getColourways() {
        /* Empty the container ready for filling */
        this.parentNode.innerHTML = "";
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/ProductItem.php",
            data: {
                colourways: "true",
                title: JSON.stringify(this.title),
                brand: JSON.stringify(this.brand),
                collection: JSON.stringify(this.collection),
                colour: JSON.stringify(this.colour)
            },
            type: "POST",
        }).done(function (reply) {
                //console.log(JSON.parse(reply));
                that.createAllItems(reply);
            }
        );
    }


    /**
     * Process the AJAX reply, parsing it and calling createItem to generate the
     * individual item for each row in the reply.
     * @param ajax_reply
     */
    private createAllItems(ajax_reply) {
        let all_products = JSON.parse(ajax_reply);
        if (all_products.length > 0) {
            /* Create each item */
            for (let i = 0; i < all_products.length; i++) {
                this.createItem(
                    all_products[i].ufid,
                    all_products[i].colour_brand,
                    all_products[i].image_main
                );
            }
            /* Create colourways header */
            this.createLabel();
        }
    }


    /**
     * Create each individual colourway item, an image of the colourway, the
     * name of the colour, and the UFID for linking to that colourways product
     * page.
     * @param ufid
     * @param colour_brand
     * @param image_main
     */
    private createItem(ufid, colour_brand, image_main) {
        /* Create container */
        let container = document.createElement('div');
        container.setAttribute('class',
            'colourway-item-container');
        container.setAttribute('id', 'colourway-item-' + ufid);

        /* Create image */
        let image = document.createElement('div');
        image.setAttribute('class', 'colourway-image');
        if (!(image_main === null)) {
            image.style.backgroundImage =
                "url('/beta/_assets/image/product/xs/" + image_main + "')";
        } else {
            image.style.backgroundImage =
                "url('/beta/_assets/image/error_product_default.png')";
        }

        /* Create label */
        let label = document.createElement('p');
        label.setAttribute('class', 'colourway-label');
        label.innerHTML = colour_brand;

        /* Append everything */
        container.appendChild(image);
        container.appendChild(label);
        this.parentNode.appendChild(container);

        /* Create event handler */
        container.addEventListener('click', function() {
            let hex_ufid = parseInt(ufid).toString(16);
            window.location.href = "index.php?ufid=0x" + hex_ufid;
        });
    }


    /**
     *
     */
    private createLabel() {
        // Create the label
        let label = document.createElement('p');
        label.setAttribute('id', 'colours-colourway-title');
        label.innerHTML = "Colourways";
        // Insert label before the colourways container.
        this.parentNode.parentElement.insertBefore(label, this.parentNode);
    }
}