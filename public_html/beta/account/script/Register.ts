/**
 * Register - Register.ts
 * This file handles the registration form and AJAX requests related to it,
 * passing credentials to register.php and handling the reply.
 *
 * @author Robert Hardy
 * @since 2018-09-02
 * Part of the Hardy'sWebSystem project.
 */
class Register {
    private formNode: HTMLElement;


    /**
     * Create a new register
     * @param formID
     * @param submitID
     */
    constructor(formID: string, submitID: string) {
        this.formNode = document.getElementById(formID);
        this.createEventListener(); // Listen for submit
    }


    /**
     *
     */
    private createEventListener() {
        let that = this;
        this.formNode.addEventListener('submit', function(event) {
            /* Stop page refresh */
            event.preventDefault();

            /* Get form values */
            let email = <HTMLInputElement>
                document.getElementById("register-email");
            let email_confirm = <HTMLInputElement>
                document.getElementById("register-emailConfirm");
            let password = <HTMLInputElement>
                document.getElementById(("register-password"));
            let password_confirm = <HTMLInputElement>
                document.getElementById(("register-passwordConfirm"));

            /* Get token then try registering with above values */
            that.tokenRegister(email.value, email_confirm.value,
                password.value, password_confirm.value);
        });
    }

    /**
     * Generate a token for CSRF protection via AJAX request to Token.php,
     * then use the token to try registering in via tryRegister().
     * @param email
     * @param email_confirm
     * @param password
     * @param password_confirm
     */
    private tokenRegister(email, email_confirm, password, password_confirm) {
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/Token.php",
            type: "POST",
        }).done(function (reply) {
                that.tryRegister(email, email_confirm,
                    password, password_confirm, reply);
            }
        );
    }


    /**
     * Use the credentials to try registering. Handle any error messages, and
     * provide feedback to the user.
     * @param email
     * @param email_confirm
     * @param password
     * @param password_confirm
     * @param token
     */
    private tryRegister(email, email_confirm, password, password_confirm, token) {
        /* Debug */
        console.log("email: " + email);
        console.log("email confirm: " + email_confirm);
        console.log("password: " + password);
        console.log("password confirm: " + password_confirm);
        console.log("token: " + token);

        $.ajax({
            url: "/beta/_global/ajax/Register.php",
            data: {
                email: email,
                email_confirm: email_confirm,
                password: password,
                password_confirm: password_confirm,
                token: token
            },
            type: "POST",
        }).done(function (reply) {
                console.log(reply);
            }
        );
    }
}