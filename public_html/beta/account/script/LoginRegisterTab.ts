/**
 * Login and Register Tab Handler - LoginRegisterTab.ts
 * This file handles the user interaction between the tabs for logging in or
 * registering on the Account page.
 *
 * @author Robert Hardy
 * @since 2018-09-01
 * Part of the Hardy'sWebSystem project.
 */
class LoginRegisterTab {
    private readonly login_tab_node: HTMLElement;
    private readonly register_tab_node: HTMLElement;
    private readonly login_form_node: HTMLElement;
    private readonly register_form_node: HTMLElement;


    /**
     * Create a new tab handler.
     * @param login_tab_ID
     * @param register_tab_ID
     * @param login_form_ID
     * @param register_form_ID
     */
    constructor(login_tab_ID: string, register_tab_ID: string,
                login_form_ID: string, register_form_ID: string) {
        this.login_tab_node = document.getElementById(login_tab_ID);
        this.register_tab_node = document.getElementById(register_tab_ID);
        this.login_form_node = document.getElementById(login_form_ID);
        this.register_form_node = document.getElementById(register_form_ID);

        /* Create event listeners */
        this.setTabEventListener("login", this.login_tab_node);
        this.setTabEventListener("register", this.register_tab_node);

        /* Set which tab is visible by default. */
        let default_tab = "login";
        this.chooseTab(default_tab);
    }


    /**
     * Set the event listeners for the tabs, so the relevant actions
     * happen when the user clicks them.
     * @param name
     * @param tab
     */
    private setTabEventListener(name: string, tab: HTMLElement) {
        let that = this;
        tab.addEventListener('click', function () {
            that.chooseTab(name);
        })
    }


    /**
     * Show a chosen tab, and hide the other.
     * @param name
     */
    public chooseTab(name: string) {
        if (name == "login") {
            /* Add the chosen class */
            this.login_tab_node.classList.add("tab-chosen");
            this.register_tab_node.classList.remove("tab-chosen");
            /* Show the form */
            this.login_form_node.style.display = "block";
            this.register_form_node.style.display = "none";
        } else if (name == "register") {
            /* Add the chosen class */
            this.login_tab_node.classList.remove("tab-chosen");
            this.register_tab_node.classList.add("tab-chosen");
            /* Show the form */
            this.login_form_node.style.display = "none";
            this.register_form_node.style.display = "block";
        }
    }
}