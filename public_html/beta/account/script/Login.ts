/**
 * Login - Login.ts
 * This file handles the login form and AJAX requests related to it, passing
 * credentials to login.php and handling the reply.
 *
 * @author Robert Hardy
 * @since 2018-09-02
 * Part of the Hardy'sWebSystem project.
 */
class Login {
    private formNode: HTMLElement;

    /**
     * Create a new login
     * @param formID
     * @param submitID
     */
    constructor(formID: string, submitID: string) {
        this.formNode = document.getElementById(formID);
        this.createEventListener(); // Listen for submit
    }


    /**
     *
     */
    private createEventListener() {
        let that = this;
        this.formNode.addEventListener('submit', function(event) {
            /* Stop page refresh */
            event.preventDefault();

            /* Get form values */
            let email = <HTMLInputElement>
                document.getElementById("login-email");
            let password = <HTMLInputElement>
                document.getElementById(("login-password"));
            let remember = <HTMLInputElement>
                document.getElementById("login-remember");

            /* Get token then try logging in with above values */
            that.tokenLogin(email.value, password.value, remember.checked);
        });
    }


    /**
     * Generate a token for CSRF protection via AJAX request to Token.php, then
     * use the token to try loggin in via tryLogin().
     * @param email
     * @param password
     * @param remember
     */
    private tokenLogin(email, password, remember) {
        let that = this;
        $.ajax({
            url: "/beta/_global/ajax/Token.php",
            type: "POST",
        }).done(function (reply) {
                that.tryLogin(email, password, remember, reply);
            }
        );
    }


    /**
     *
     */
    private tryLogin(email, password, remember, token) {
        /* Debug */
        console.log("email: " + email);
        console.log("password: " + password);
        console.log("remember: " + remember);
        console.log("token: " + token);

        $.ajax({
            url: "/beta/_global/ajax/Login.php",
            data: {
                email: email,
                password: password,
                remember: remember,
                token: token
            },
            type: "POST",
        }).done(function (reply) {
                console.log("Login attempt success? " + reply);
                window.location.reload(); // Refresh
            }
        );
    }
}