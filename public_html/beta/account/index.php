<!DOCTYPE HTML>
<html lang="en">
<head>
    <!--
    Account - index.php
    This file defines the account page, where users can sign in or register.
    @author Robert Hardy
    @since 2018-08-31
    Part of the Hardy'sWebSystem project.
    -->
    <title>Jones &amp Hicks - Account</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- JavaScript Files -->
    <script src="../_global/script/class/NavSite.js"></script>
    <script src="../_global/script/class/NavAccount.js"></script>
    <script src="../_global/script/class/NavBasket.js"></script>
    <script src="../_global/script/class/NavFooter.js"></script>
    <script src="script/LoginRegisterTab.js"></script>
    <script src="script/Login.js"></script>
    <script src="script/Register.js"></script>

    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-site.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-side.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/nav-account.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/header.css">
    <link rel="stylesheet" type="text/css" href="../_global/style/default.css">
    <link rel="stylesheet" type="text/css" href="style/_main.css">
    <link rel="stylesheet" type="text/css" href="style/login-register.css">
    <link rel="stylesheet" type="text/css" href="style/form.css">
    <link rel="stylesheet" type="text/css" href="style/profile.css">

    <!-- Initialise PHP -->
    <?php
    require_once __DIR__ . '/../../../application/core/initialise.php';
    ?>
</head>
<body>
<!-- Layout the grid -->
<div id="grid-page">

    <!-- Header -->
    <header class="border grid-box" id="header">
        <!-- Site title and buttons for account and basket -->
        <div class="border" id="header-title-account-basket-row">
            <h1 id="header-title">
                Jones &amp Hicks
            </h1>
            <div class="border inner-box" id="nav-account-container">
                Account
            </div>
            <div class="border inner-box" id="nav-basket-container">
                Basket
            </div>
        </div>
        <!-- Site Nav Bar -->
        <div class="border" id="header-nav-site-row">
            <nav class="border inner-box" id="nav-site-container">
                <!-- Populate by script -->
            </nav>
            <div class="border inner-box spacer-nav">
                <i>Spacer</i>
            </div>
        </div>
    </header>

    <!-- Log in or Register  -->
    <main class="border grid-box" id="main">
        <?php
        $user = new User();
        if ($user->getIsLoggedIn()) {
            include "include/profile.php";
        } else {
            include "include/login_register.php";
        }
        ?>
    </main>

    <!-- Footer -->
    <footer class="border grid-box" id="footer">
        Footer
        <div class="border inner-box" id="nav-footer-container">
            Footer nav
        </div>
    </footer>

    <!-- Scripts for handling the above -->
    <script>
        /* Header Navigation */
        let navSite = new NavSite("nav-site-container");
        let navAccount = new NavAccount("nav-account-container");
        let navBasket = new NavBasket("nav-basket-container");


        <?php
        /* Initialise certain scripts depending
        on if user is logged in or not. */
        $user = new User();
        if($user->getIsLoggedIn()) {
            /* User is logged in, populate profile action list. */
            ?>
            navAccount.createList(true, <?= $user->getPermission(); ?>,
                "profile-actions-container");
            <?php
        } else if (Input::exists('get') &&
            Input::get('action') === 'sign out') {
            ?>
            /* User has just signed out, clear any GET from URL */
            navAccount.createList(false, 0, "profile-actions-container");
            <?php
        } else if (Input::exists('get') &&
                Input::get('action') === 'register') {
            ?>
            /* Login or Register Tabs */
            let tabHandler = new LoginRegisterTab("tab-login",
                "tab-register", "form-login", "form-register");

            /* Login and Register Handlers */
            let login = new Login("form-login", "submit-login");
            let register = new Register("form-register", "submit-register");

            /* User has clicked register link so show register tab */
            tabHandler.chooseTab("register");
            <?php
        } else {
            /* User not logged in, initialise scripts
            for the login and register forms */
            ?>
            /* Login or Register Tabs */
            let tabHandler = new LoginRegisterTab("tab-login", "tab-register",
                "form-login", "form-register");

            /* Login and Register Handlers */
            let login = new Login("form-login", "submit-login");
            let register = new Register("form-register", "submit-register");
            <?php
        }
        ?>

        /* Clear any GET from URL */
        if (typeof window.history.pushState === 'function') {
            window.history.pushState({}, "Hide",
                '<?php echo $_SERVER['PHP_SELF']; ?>');
        }

        /* Footer Navigation */
        let footer = new NavFooter("nav-footer-container");
    </script>
</div>
</body>
