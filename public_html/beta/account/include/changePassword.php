<?php
/**
 * Change Password - changePassword.php
 * @author Robert Hardy
 * @since 2018-07-27
 *
 * This file defines the ability for a user to change their password.
 */
require_once __DIR__ . '/../../../../application/core/initialise.php';

$user = new User();

// Check if user logged in, if not then redirect back home.
if (!$user->getIsLoggedIn()) {
    Redirect::to('/../index.php');
}

if (Input::exists()) {
    if (Token::check(Input::get('token'))) {

        $validate = new Validate();
        $validate->check($_POST, array(
            'existingPassword' => array(
                // Rules for password
                'required' => true,
                'min' => 6
            ),
            'password' => array(
                // Rules for password
                'required' => true,
                'min' => 6 // TODO: method for ensuring strong password?
            ),
            'passwordConfirm' => array(
                // Rules for password confirmation
                'required' => true,
                'min' => 6,
                'matches' => 'password'
            )
        ));

        // Check if passes validation
        if ($validate->getPassed()) {
            if (!(password_verify(Input::get('existingPassword'),
                $user->getData()->password))) {
                echo "Your current password has been entered incorrectly";
            } else {
                // Update user details
                try {
                    $hash = Hash::make(Input::get('password'));
                    $user->update(array('password' => $hash));
                    Session::flash('home', "Your password has been updated!");
                    Redirect::to('../index.php');
                } catch (Exception $e) {
                    die($e->getMessage());
                }
            }
        } else {
            // Output errors
            foreach ($validate->getErrors() as $error) {
                echo $error . "<br>";
            }
        }
    }
}
?>

<!-- Build the update form -->
<form action="" method="post">
    <div class="field">
        <label for="existingPassword">Current Password</label>
        <input type="password" name="existingPassword" id="existingPassword" value="">
    </div>
    <div class="field">
        <label for="password">Password</label>
        <input type="password" name="password" id="password" value="">
    </div>
    <div class="field">
        <label for="passwordConfirm">Password Confirmation</label>
        <input type="password" name="passwordConfirm" id="passwordConfirm" value="">
    </div>
    <input type="submit" value="Update">
    <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
</form>
