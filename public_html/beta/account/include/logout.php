<?php
/**
 * Logout
 * @author Robert Hardy
 * @since 2018-07-26
 *
 * This file defines the logging out site.
 */
require_once __DIR__ . '/../../../../application/core/initialise.php';

// Create a new user from the current session holder, and then log them out.
$user = new User();
$user->logout();

// Redirect home.
Redirect::to('../index.php');