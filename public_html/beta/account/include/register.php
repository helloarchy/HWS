<!--
Register Form - register.php
This file defines the register form.
@author Robert Hardy
@since 2018-09-01
Part of the Hardy'sWebSystem project.
-->
<form method="post" class="login-register-form">
    <!-- Email -->
    <div class="field">
        <label for="register-email">Email</label>
        <input type="email" name="register-email" id="register-email"
               value="<?php echo escape(Input::get('email')); ?>" required>
    </div>
    <!-- Email Confirm -->
    <div class="field">
        <label for="register-emailConfirm">Email Confirm</label>
        <input type="email" name="register-emailConfirm" id="register-emailConfirm"
               value="<?php echo escape(Input::get('email')); ?>" required>
    </div>
    <!-- Password -->
    <div class="field">
        <label for="register-password">Password</label>
        <input type="password" name="register-password"
               id="register-password" value="" required>
    </div>
    <!-- Password Confirm -->
    <div class="field">
        <label for="register-passwordConfirm">Confirm Password</label>
        <input type="password" name="register-passwordConfirm"
               id="register-passwordConfirm" value="" required>
    </div>
    <!-- Submit -->
    <input type="submit" name="submit" value="register" id="submit-register">
</form>
