<?php
/**
 * Update - update.php
 * @author Robert Hardy
 * @since 2018-07-27
 *
 * This file defines the update profile information form page for the site.
 */
require_once __DIR__ . '/../../../../application/core/initialise.php';

$user = new User();

// Check if user logged in, if not then redirect back home.
if (!$user->getIsLoggedIn()) {
    Redirect::to('/../index.php');
}

/* Handle input provided token valid (to stop cross site request forgery */
if (Input::exists()) {
    if (Token::check(Input::get('token'))) {
        /* Now validate input */
        $validate = new Validate();
        $validate->check($_POST, array(
            'firstName' => array(
                // Rules for first name
                'required' => true,
                'min' => 2,
                'max' => 255 // limit of database
            ),
            'lastName' => array(
                // Rules for last name
                'required' => true,
                'min' => 2,
                'max' => 255 // limit of database
            )
            /*'email' => array(
                // Rules for email
                'required' => true,
                'min' => 6, // goo.gl/qUat5B
                'max' => 255,
                'unique' => 'user' // Unique in the users table
            )*/
        ));

        // Check if passes validation
        if ($validate->getPassed()) {
            // Update user details
            try {
                $user->update(array(
                    'firstName' => Input::get('firstName'),
                    'lastName' => Input::get('lastName')
                    /*'email' => Input::get('email'))*/
                ));
                Session::flash('home', "Your details have been updated!");
                Redirect::to('../index.php');
            } catch (Exception $e) {
                die($e->getMessage());
            }
        } else {
            // Output errors
            foreach ($validate->getErrors() as $error) {
                echo $error . "<br>";
            }
        }
    }
}


?>
<!-- Build the update form -->
<form action="" method="post">
    <div class="field">
        <label for="firstName">First Name</label>
        <input type="text" name="firstName" id="firstName"
               value="<?php echo escape($user->getData()->firstname); ?>">
    </div>
    <div class="field">
        <label for="lastName">Last Name</label>
        <input type="text" name="lastName" id="lastName"
               value="<?php echo escape($user->getData()->lastname); ?>">
    </div>
    <!--<div class="field">
        <label for="email">Email</label>
        <input type="text" name="email" id="email"
               value="<?php /*echo escape($user->getData()->email); */ ?>">
    </div>-->
    <input type="submit" value="Update">
    <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
</form>
