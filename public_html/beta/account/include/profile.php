<!--
Profile Page - profile.php
This file defines a users profile page, where many actions can take place which
are only available to a signed in user.
@author Robert Hardy
@since 2018-09-06
Part of the Hardy'sWebSystem project.
-->
<?php
/* Get the user data */
$user = new User();
$data = $user->getData();
/* See if user is an Admin, if so, redirect to admin page. */
if ($user->getPermission() >= 2) {
    Redirect::to("/beta/admin/index.php");
}
?>
<!-- Container -->
<div class="border" id="profile-container">
    <?php
    if (Input::exists('get') &&
        Input::get('action') === 'sign out') {
        $user->logout();
        ?>
        <!-- You have been signed out message -->
        <div id="profile-signed-out">
            You have been signed out successfully.
            <div id="profile-signed-out-links">
                <a href="../index.php">home</a>
                <a href="../explore/index.php">explore</a>
            </div>
        </div>
        <!-- Actions Panel -->
        <div class="border" id="profile-actions-container">
            <!-- Populated via script -->
        </div>
        <?php
    } else if (Input::exists('get') &&
        Input::get('action') === 'orders') {
        ?>
        <!-- Show order history -->
        <div id="profile-orders-container">
            You have no orders [placeholder]
        </div>
        <!-- Actions Panel -->
        <div class="border" id="profile-actions-container">
            <!-- Populated via script -->
        </div>
        <?php
    } else {
        ?>
        <!-- Show details -->
        <div id="profile-details-container">
            <h3>Account Details</h3>
            <p>First name: <?php echo escape($data->firstname); ?></p>
            <p>Last name: <?php echo escape($data->lastname); ?></p>
            <p>email: <?php echo escape($data->email); ?></p>
            <p>account created: <?php echo escape($data->created); ?></p>
        </div>
        <!-- Actions Panel -->
        <div class="border" id="profile-actions-container">
            <!-- Populated via script -->
        </div>
        <?php
    }
    ?>
</div>

