<!--
Login and Register Tab Forms - login_register.php
This file defines the tabs and inclusion of the login and register form.
@author Robert Hardy
@since 2018-09-06
Part of the Hardy'sWebSystem project.
-->
<div class="border" id="login-register-container">
    <!-- Tabs -->
    <div id="tab-container">
        <!-- Login Tab -->
        <div class="tab-login-register" id="tab-login">
            Login
        </div>
        <!-- Vertical bar -->
        <div id="tab-divider"></div>
        <!-- Register Tab -->
        <div class="tab-login-register" id="tab-register">
            Register
        </div>
    </div>

    <!-- Forms -->
    <div id="form-login">
        <!-- Login Form -->
        <?php include "login.php"; ?>
    </div>
    <div id="form-register">
        <!-- Register Form -->
        <?php include "register.php"; ?>
    </div>
</div>