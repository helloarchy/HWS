<!--
Login Form - login.php
This file defines the login form.
@author Robert Hardy
@since 2018-09-01
Part of the Hardy'sWebSystem project.
-->
<form method="post" class="login-register-form" id="form-login">
    <!-- Email -->
    <div class="field">
        <label for="login-email">Email</label>
        <input type="email" name="login-email" id="login-email"
               required autofocus>
    </div>
    <!-- Password -->
    <div class="field">
        <label for="login-password">Password</label>
        <input type="password" name="login-password" id="login-password"
               value="" required>
    </div>
    <!-- Remember -->
    <div class="field">
        <label for="login-remember">Remember</label>
        <input type="checkbox" name="login-remember" id="login-remember">
    </div>
    <!-- Submit -->
    <input type="submit" name="submit" value="login" id="submit-login">
</form>