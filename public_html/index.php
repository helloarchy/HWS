<!DOCTYPE html>
<html lang="en">
<head>
    <!--
     index.php
     The temporary landing page while the site is under construction.
     @author Robert Hardy
     @since 2018-09-18
     Part of the HardysWebSystem project.
    -->
    <title>Jones &amp Hicks - Coming Soon!</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>

    <!-- JavaScript Files -->
    <script src="landing/script/script.js"></script>

    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="beta/_global/style/default.css">
    <link rel="stylesheet" type="text/css" href="landing/style/_main.css">
    <link rel="stylesheet" type="text/css" href="landing/style/header.css">
    <link rel="stylesheet" type="text/css" href="landing/style/footer.css">
    <link rel="stylesheet" type="text/css" href="landing/style/content.css">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Muli|Ovo|Bellefair"
          rel="stylesheet">

    <?php
    // Initialise everything from init.
    require_once __DIR__ . '/../application/core/initialise.php';
    ?>
</head>

<body>
<!-- Layout the grid -->
<div id="grid-page">

    <!-- Header -->
    <header class="" id="header">
        <h1 id="header-title">
            jones &amp hicks
        </h1>

        <!-- Site Nav Bar -->
        <div class="" id="header-nav-site">
            <a class="nav-link" href="#about-container">
                about
            </a>
            <div class="nav-link-divider">
                <!-- vertical bar divider -->
            </div>
            <a class="nav-link" href="#contact-container">
                contact
            </a>
        </div>
    </header>

    <!-- Main -->
    <main class="border" id="main">
        <!-- Welcome image? -->
        <div class="content-image-container" id="welcome-image">
            <!-- background image -->
        </div>
        <!-- Welcome -->
        <div class="content-text-container" id="welcome-container">
            <h2>
                welcome
            </h2>
            <p>
                Jones & Hicks is a long-established soft furnishings company,
                creating timeless and elegant curtains, blinds and all things
                fabric. We offer a comprehensive service, providing you with
                beautiful furnishings, created with care and attention to
                detail. As well as a personal service that is second to none.
            </p>
            <p>
                We have always been passionate about fabric, the different
                moods you can create, the pleasure of choosing from seemingly
                infinite designs and texture, colours and styles. Soft
                furnishings can transform even the most unpromising rooms into
                somewhere special, a place to call home.
            </p>
            <p>
                At Jones & Hicks, we have the skills and expertise to provide
                you with exactly what you want, even if you are not quite sure
                what you want!
            </p>
        </div>
        <!-- About -->
        <div class="content-text-container" id="about-container">
            <h2>
                about us
            </h2>
            <p>
                Founded in 1998, we had a shared passion to create something
                special, to make the very best handmade soft furnishings,
                accompanied with excellent quality of service and advice.
            </p>
            <p>
                Alongside our local client base, we have always worked with
                interior designers based in London, who demand hand crafted
                curtains and roman blinds of a consistently high quality and
                delivered on time, every time.
            </p>
            <p>
                It’s a lovely feeling to know our curtains and blinds adorn
                many homes, hotels, and businesses throughout London, the UK,
                Europe and America. Our skills and expertise have developed
                over 20 years, and our love of all thing’s fabric continues
                to keep the creative passion flowing. Simply put, we love what
                we do.
            </p>
        </div>
        <!-- Contact -->
        <div class="content-text-container" id="contact-container">
            <h2>
                contact us
            </h2>
            <p>
                For further information regarding our services, or to arrange
                an appointment to discuss how we can be of help,
            </p>
            <p>
                Call us on ...........................
            </p>
            <p>
                Email us @ ...............................
            </p>
            <p>
                We welcome residential as well as commercial enquiries.
            </p>
        </div>
    </main>

    <!-- Footer -->
    <footer class="border" id="footer">
        <p id="footer-text">jones & hicks 2018</p>
        <div id="socialIconsContainer">
            <a href="#">
                <img src="landing/_assets/icons/social_media/if_etsy_386620.svg">
            </a>
            <a href="#">
                <img src="landing/_assets/icons/social_media/if_facebook_386622.svg">
            </a>
            <a href="#">
                <img src="landing/_assets/icons/social_media/if_google_plus_386644.svg">
            </a>
            <a href="#">
                <img src="landing/_assets/icons/social_media/if_instagram_386648.svg">
            </a>
            <a href="#">
                <img src="landing/_assets/icons/social_media/if_twitter_386736.svg">
            </a>
            <a href="#">
                <img src="landing/_assets/icons/social_media/if_whatsapp_386747.svg">
            </a>
        </div>
        <div class="border inner-box" id="nav-footer-container">
            Footer nav
        </div>
    </footer>

    <!-- Scripts for handling the above -->
    <script>
    </script>
</div>

</body>
</html>