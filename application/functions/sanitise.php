<?php
/**
 * Sanitiser - sanitise.php
 * @author Robert Hardy
 * @since 2018-06-15
 *
 * This file handles the sanitising of data coming in and out of the database,
 * escaping data as it comes out. This provides security against SQL injections
 * and other possible vulnerabilities.
 *
 * @param $string = to be escaped
 * @return string = that is escaped
 */
function escape($string)
{
    // ENT_QUOTES for single and double quotes.
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}