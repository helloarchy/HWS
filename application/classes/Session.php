<?php

/**
 * Class Session
 * @author Robert Hardy
 * @since 2018-06-15
 *
 * This file defines a users individual session across the site, used for consistency
 * across pages which reflect their individual experience.
 */
class Session
{
    /**
     * Put in a session, with names and values.
     * @param $name
     * @param $value
     * @return mixed
     */
    public static function put($name, $value)
    {
        return $_SESSION[$name] = $value;
    }


    /**
     * If there is a session of this name, return true, else false.
     * @param $name
     * @return bool
     */
    public static function exists($name)
    {
        return (isset($_SESSION[$name])) ? true : false;
    }


    /**
     * Get the session by its name.
     * @param $name
     * @return mixed
     */
    public static function get($name)
    {
        return $_SESSION[$name];
    }


    /**
     * If session is set, unset it, effectively deleting the session.
     * @param $name
     */
    public static function delete($name)
    {
        if (self::exists($name)) {
            unset($_SESSION[$name]);
        }
    }


    /**
     * Flash data to user (show them something once then gone on refresh)
     * @param $name
     * @param string $string
     * @return mixed
     */
    public static function flash($name, $string = '')
    {
        // If name exists, return the data and delete it (so gone on refresh)
        if (self::exists($name)) {
            $session = self::get($name);
            self::delete($name);
            return $session;
        } else {
            self::put($name, $string);
        }
        return '';
    }
}