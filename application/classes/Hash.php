<?php

/**
 * Class Hash
 * @author Robert Hardy
 * @since 2018-06-22
 *
 * This file defines the hashing functionality using across the web system for
 * hiding sensitive data when passing and retrieving from the database.
 */
class Hash
{
    /**
     * Create a randomly generated string of data for replacing plain-text sensitive
     * data with a completely random illegible bunch of characters.
     * @param $string
     * @return string
     */
    public static function make($string)
    {
        return password_hash($string, PASSWORD_DEFAULT);
    }


    /**
     * Check if hash is unique
     * @return string
     *
     */
    public static function unique()
    {
        return self::make(uniqid());
    }


    /**
     * Verify that a given string matches a hash
     * @param $string
     * @param $hash
     * @return bool
     */
    public static function verify($string, $hash)
    {
        // Check if match
        if (password_verify($string, $hash)) {
            // Check if password needs rehashing to comply with updated standards
            if (password_needs_rehash($hash, PASSWORD_DEFAULT)) {
                // Rehash
                $newHash = password_hash($string, PASSWORD_DEFAULT);
                // TODO: SET THE USERS PASSWORD TO THE NEW HASHED PASSWORD
            }
            return true; // Verified
        }
        return false; // No match
    }
}