<?php

/**
 * Product.php
 * Defines a Product within the system, ie, a fabric, lining, or interlining.
 * @author Robert Hardy
 * @since 2018-09-10
 * Part of the Hardy'sWebSystem project.
 */
class Product
{
    private $reference;
    private $title;
    private $brand;
    private $collection;
    private $price_current;
    private $price_original;
    private $colour_brand;
    private $colour_hardys;
    private $description;
    private $width;
    private $pattern;
    private $pattern_repeat;
    private $pattern_match;
    private $type;
    private $retardancy;
    private $application;
    private $composition;
    private $supplier;
    private $available;
    private $image;

    /**
     * Product constructor.
     * @param $title
     * @param $price_current
     * @param $price_original
     * @param $colour_hardys
     * @param $width
     */
    public function __construct($title, $price_current, $price_original,
                                $colour_hardys, $width)
    {
        $this->title = $title;
        $this->price_current = $price_current;
        $this->price_original = $price_original;
        $this->colour_hardys = $colour_hardys;
        $this->width = $width;
    }



}