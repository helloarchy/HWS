<?php

/**
 * Class Redirect
 * @author Robert Hardy
 * @since 2018-07-05
 *
 * This file defines how users are redirected around the web pages.
 */
class Redirect
{

    /**
     * Redirected a user to a given location
     * @param null $location to redirect to.
     */
    public static function to($location = null)
    {
        //If location is defined, then header
        if ($location) {
            // Check if location numeric error code
            if (is_numeric($location)) {
                // Switch to allow expansion later
                switch ($location) {
                    case 404:
                        header('Location: /beta/error/404.php');
                        //include __DIR__ . '/../../public_html/beta/error/404.php';
                        exit();
                        break;
                }
            }
            header('Location: ' . $location);
            exit();
        }
    }
}