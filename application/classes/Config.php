<?php

/**
 * Class Config
 * @author Robert Hardy
 * @since 2018-06-15
 *
 * This file allows us to access the GLOBALS data set up during initialisation by
 * simply using slashes instead of 3D arrays. Allows us to avoid repeating ourselves
 * and protects against name changing.
 */
class Config
{
    /**
     * @param null $path
     * @return mixed
     */
    public static function get($path = null)
    {
        if ($path) {
            $config = $GLOBALS['config']; // So we don't have to repeat ourselves.
            $path = explode('/', $path); // Explode by '/'
            // Loop for each exploded part
            foreach ($path as $bit) {
                // See if set, if it is, set config to that bit.
                if (isset($config[$bit])) {
                    $config = $config[$bit];
                }
            }
            return $config;
        }
        return false; // If nothing exists for this part.
    }
}