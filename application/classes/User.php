<?php

/**
 * Class User
 * @author Robert Hardy
 * @since 2018-06-22
 *
 * This file defines a single user of the web system.
 */
class User
{
    private $db; // The database
    private $data; // Store the data from the database
    private $sessionName; // Get the session
    private $cookieName; // For storing 'remember me' log in.
    private $isLoggedIn;


    /**
     * User constructor.
     * @param null $user the user
     */
    public function __construct($user = null)
    {
        $this->db = Database::getInstance();
        $this->sessionName = Config::get('session/session_name');
        $this->cookieName = Config::get('remember/cookie_name');

        /* Check if the session exists, but see if user exists first.
        Otherwise we can get the user that isn't currently logged in. */
        if (!$user) {
            // is user logged in, is there a session
            if (Session::exists($this->sessionName)) {
                $user = Session::get($this->sessionName);

                // Set the user as being logged in.
                if ($this->find($user)) {
                    $this->isLoggedIn = true;
                } else {
                    $this->isLoggedIn = false;
                }
            }
        } else {
            // If the user has been defined then find and pair
            $this->find($user);
        }
    }


    /**
     * Create a user
     * @param array $fields
     * @throws Exception
     */
    public function create($fields = array())
    {
        /* If not successful, throw error */
        $success = $this->db->insert('user', $fields);
        if (!($success)) {
            throw new Exception('There was a problem creating an account.');
        }
    }


    /**
     * Log the user in.
     * @param $email
     * @param $password
     * @param $remember
     * @return bool
     */
    public function login($email = null, $password = null, $remember = false)
    {

        /* Log user in from cookie (so no username or password defined), otherwise
        log in as standard using details given via log in form. */
        if (!$email && !$password && $this->exists()) {
            // Create a session from the data already existing within this user.
            Session::put($this->sessionName, $this->getData()->uuid);
        } else {
            $user = $this->find($email);
            // Check if email and password exist
            if ($user) {
                if (password_verify($password, $this->getData()->password)) {
                    // Assign user to their session
                    Session::put($this->sessionName, $this->getData()->uuid);

                    /* If user chooses to be remembered when logging in, then set a
                    cookie to remember them. */
                    if ($remember) {
                        $hash = Hash::unique();
                        $hashCheck = $this->db->get('session',
                            array('user', '=', $this->getData()->uuid));

                        /* If no hash in session database (count = 0) */
                        if (!$hashCheck->count()) {
                            $this->db->insert('session', array(
                                'user' => $this->getData()->uuid,
                                'hash' => $hash));
                        } else {
                            // Set the hash to the first hash check result
                            $hash = $hashCheck->first()->hash;
                        }
                        // Set the cookie
                        Cookie::put($this->cookieName, $hash,
                            Config::get('remember/cookie_expiry'));
                    }
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * See if a given value exists in the database
     * @param null $user
     * @return bool
     */
    public function find($user = null)
    {
        if ($user) {
            // Structure query
            $field = is_numeric($user) ? 'uuid' : 'email';
            $data = $this->db->get('user', array($field, '=', $user));

            // Check if at least one result
            if ($data->count()) {
                // Set the user data to the first result
                $this->data = $data->first();
                return true;
            }
        }
        return false;
    }


    /**
     * Update the user details of a given user.
     * @param array $fields
     * @param null $uuid
     * @throws Exception
     */
    public function update($fields = array(), $uuid = null)
    {
        /* If no ID given, then update the current user. */
        if (!$uuid && $this->getIsLoggedIn()) {
            $uuid = $this->getData()->uuid;
        }

        /* Try to update database record, if error, throw exception */
        if (!$this->db->update('user', $uuid, $fields)) {
            throw new Exception("There was a problem updating");
        }
    }


    /**
     * Check if user exists
     */
    public function exists()
    {
        // Check if any data exists in this array
        return (!empty($this->getData() ? true : false));
    }


    /**
     * Log the current user out of the system.
     * This is achieved by simply deleting the users session and cookie.
     */
    public function logout()
    {
        // Delete cookie from database, then delete instances.
        $this->db->delete('session', array('user', '=', $this->getData()->uuid));

        Session::delete($this->sessionName);
        Cookie::delete($this->cookieName);
    }


    /**
     * Check if the user has a given permission level.
     * @param int $permissionLevel
     * @return bool
     */
    public function hasPermission(int $permissionLevel)
    {
        // Get the users class
        $class = $this->db->get('class', array('user', '=',
            $this->getData()->uuid));
        // If they have a class, extract the level from it.
        if ($class->count()) {
            $level = $class->first()->level;
            // Check level is at or above the required permission level
            if ($level >= $permissionLevel) {
                return true;
            }
        }
        return false;
    }


    /**
     * Get the users permission level.
     * @return int
     */
    public function getPermission()
    {
        // Get the users class
        $class = $this->db->get('class', array('user', '=',
            $this->getData()->uuid));
        // If they have a class, extract the level from it.
        $level = 1;
        if ($class->count()) {
            $level = $class->first()->level;
        }
        return $level;
    }


    /**
     * Get the data
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * @return mixed
     */
    public function getIsLoggedIn()
    {
        return $this->isLoggedIn;
    }
}