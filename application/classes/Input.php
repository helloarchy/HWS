<?php

/**
 * Class Input
 * @author Robert Hardy
 * @since 2018-06-15
 *
 * This file defines the input of form page data for the site.
 */
class Input
{
    /**
     * Check in input data exists
     * @param string $type
     * @return bool
     */
    public static function exists($type = 'post')
    {
        // Switch on the type to check cases
        switch ($type) {
            case 'post':
                return (!empty($_POST)) ? true : false;
                break;
            case 'get':
                return (!empty($_GET)) ? true : false;
                break;
            default:
                return false;
                break;
        }
    }


    /**
     * Get the input data if it exists, else return empty string.
     * @param $item
     * @return string
     */
    public static function get($item)
    {
        // Check if set and available, if so, return
        if (isset($_POST[$item])) {
            return $_POST[$item];
        } else if (isset($_GET[$item])) {
            return $_GET[$item];
        }
        return ''; // Else return an empty string.
    }
}