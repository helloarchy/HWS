<?php

/**
 * Cookie.php
 * @author Robert Hardy
 * @since 2018-06-15
 *
 * This file defines the setting of cookies on a users device, which is then used
 * by the web system to automate preferences and remembering log ins.
 */
class Cookie
{
    /**
     * Check if a cookie exists
     * @param $name
     * @return bool
     */
    public static function exists($name)
    {
        return (isset($_COOKIE[$name])) ? true : false;
    }


    /**
     * Get a cookie by its name
     * @param $name
     * @return mixed
     */
    public static function get($name)
    {
        return $_COOKIE[$name];
    }


    /**
     * @param $name
     * @param $value
     * @param $expiry
     * @return bool
     */
    public static function put($name, $value, $expiry)
    {
        /* Set the cookie, append expiry time to the current time, and
        set the cookies path to the directories path. */
        $expiry = (int) (time() + $expiry);
        if (setcookie($name, $value, $expiry, '/')) {
            return true;
        }
        return false;
    }


    /**
     * @param $name
     */
    public static function delete($name)
    {
        /* Replace the current cookie with negative values */
        self::put($name, '', time() - 1);
    }
}