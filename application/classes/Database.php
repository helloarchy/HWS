<?php

/**
 * Class Database
 *
 * @author Robert Hardy
 * @since 2018-06-15
 *
 * This SINGLETON class defines a wrapper for the PDO, php data objects, to allow
 * an abstracted way of working with the database across the whole system,
 * maximising re-usability. Security measures are taken at every possibility,
 * protecting against SQL injection by using prepared statements.
 */
class Database
{
    // Keep an instance of the database rather than reconnecting each time
    private static $instance = null;
    private $pdo;
    private $query;
    private $error = false;
    private $result;
    private $count = 0;


    /**
     * Create a new instance of the Database class.
     */
    private function __construct()
    {
        try {
            /* Create a new PDO using DSN (driver, host, database), username,
            and password, all pulled from the GLOBALS via Config class. */
            $this->pdo = new PDO(
                'mysql:host=' . Config::get('mysql/host') .
                ';dbname=' . Config::get('mysql/database') . ';',
                Config::get('mysql/username'),
                Config::get('mysql/password')
            );
            //echo 'Connected to database.';
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }


    /**
     * Get a new instance of database, or get existing instance if one exists.
     * @return Database|null
     */
    public static function getInstance()
    {
        // Prevent multiple instances from being created.
        if (!isset(self::$instance)) {
            self::$instance = new Database();
        }
        return self::$instance;
    }


    /**
     * A generic method for querying the database. Makes use of prepared statements
     * for security.
     * @param $sql = the query string.
     * @param array $params = the arguments for inserting into prepared statement.
     * @return Database
     */
    public function query($sql, $params = array())
    {
        $this->error = false; // Set back to false if multiple queries.
        // Check if query prepared properly.
        if ($this->query = $this->pdo->prepare($sql)) {
            //echo 'Prepared successfully!';
            /* Check if any params and loop through them, bind param to its xth
            position within the query. */
            $x = 1;
            if (count($params)) {
                foreach ($params as $param) {
                    $this->query->bindValue($x, $param);
                    $x++;
                }
            }
            // Execute query (whether any params or not)
            if ($this->query->execute()) {
                //echo "Query legal, executed!";
                // Store results using PDO's fetchAll method, and get as an object
                $this->result = $this->query->fetchAll(PDO::FETCH_OBJ);
                $this->count = $this->query->rowCount();
            } else {
                //echo "Query illegal, not executed!";
                $this->error = true; // Flag error.
            }
        }
        return $this;
    }


    /**
     * A generic method for querying the database. Makes use of prepared statements
     * for security.
     * @param $sql = the query string.
     * @param array $params = the arguments for inserting into prepared statement.
     * @return Database
     */
    public function getColumn($sql, $params = array())
    {
        $this->error = false; // Set back to false if multiple queries.
        // Check if query prepared properly.
        if ($this->query = $this->pdo->prepare($sql)) {
            //echo 'Prepared successfully!';
            /* Check if any params and loop through them, bind param to its xth
            position within the query. */
            $x = 1;
            if (count($params)) {
                foreach ($params as $param) {
                    $this->query->bindValue($x, $param);
                    $x++;
                }
            }
            // Execute query (whether any params or not)
            if ($this->query->execute()) {
                //echo "Query legal, executed!";
                // Store results using PDO's fetchAll method, and get as an object
                $this->result = $this->query->fetchAll(PDO::FETCH_COLUMN);
                $this->count = $this->query->rowCount();
            } else {
                //echo "Query illegal, not executed!";
                $this->error = true; // Flag error.
            }
        }
        return $this;
    }


    /**
     * A generic method for querying the database. Makes use of prepared statements
     * for security.
     * @param $sql = the query string.
     * @param array $params = the arguments for inserting into prepared statement.
     * @return Database
     */
    public function getRows($sql, $params = array())
    {
        $this->error = false; // Set back to false if multiple queries.
        // Check if query prepared properly.
        if ($this->query = $this->pdo->prepare($sql)) {
            //echo 'Prepared successfully!';
            /* Check if any params and loop through them, bind param to its xth
            position within the query. */
            $x = 1;
            if (count($params)) {
                foreach ($params as $param) {
                    $this->query->bindValue($x, $param);
                    $x++;
                }
            }
            // Execute query (whether any params or not)
            if ($this->query->execute()) {
                //echo "Query legal, executed!";
                // Store results using PDO's fetchAll method, and get as an object
                $this->result = $this->query->fetchAll(PDO::FETCH_ASSOC);
                $this->count = $this->query->rowCount();
            } else {
                //echo "Query illegal, not executed!";
                $this->error = true; // Flag error.
            }
        }
        return $this;
    }


    /**
     * Provide a generic base for running an SQL query
     * @param $action
     * @param $table
     * @param array $where
     * @return $this|bool
     */
    public function action($action, $table, $where = array())
    {
        // Check if we have field, operator, value for where clause
        if (count($where) === 3) {
            $operators = array('=', '>', '<', '<=', '>=');
            $field = $where[0];
            $operator = $where[1];
            $value = $where[2];
            // Check if operator in array
            if (in_array($operator, $operators)) {
                // Check if error, if not, return
                $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";
                if (!$this->query($sql, array($value))->error()) {
                    return $this;
                }
            }
        }
        return false;
    }


    /**
     * A generic means of getting data from a table, using action above.
     * @param $table
     * @param $where
     * @return bool|Database
     */
    public function get($table, $where)
    {
        return $this->action('SELECT *', $table, $where);
    }


    /**
     * A generic means of deleting from a table, using action above.
     * @param $table
     * @param $where
     * @return bool|Database
     */
    public function delete($table, $where)
    {
        return $this->action('DELETE', $table, $where);
    }


    /**
     * Get all the results of the query if there are any.
     * @return mixed
     */
    public function results()
    {
        return $this->result;
    }


    /**
     * Get the first result.
     * @return mixed
     */
    public function first()
    {
        return $this->results()[0];
    }


    /**
     * Insert data into our database, dynamic and responsive to varying fields.
     * @param $table
     * @param array $fields
     * @return bool
     */
    public function insert($table, $fields = array())
    {
        // Check if we actually have any fields
        if (count($fields)) {
            $keys = array_keys($fields); // Get the keys of the array (index titles)
            $values = ''; // empty string
            $x = 1;

            foreach ($fields as $field) {
                $values .= '?';
                // Add a comma between fields
                if ($x < count($fields)) {
                    $values .= ', ';
                }
                $x++;
            }

            // Query for insert, implode with back ticks so all entries have them
            $sql = "INSERT INTO {$table} (`" . implode('`, `', $keys) . "`) 
                VALUES ({$values})";
            //echo $sql;

            // Pass in the query now they all have values, return true if no error.
            $success = $this->query($sql, $fields)->error();
            if (!($success)) {
                return true;
            }
        }
        return false; // Insert unsuccessful
    }


    /**
     * Update a particular row in a given table, using given id and fields.
     * @param $table
     * @param $id
     * @param $fields
     * @return bool
     */
    public function update($table, $id, $fields)
    {
        $set = '';
        $x = 1;

        foreach ($fields as $name => $value) {
            $set .= "{$name} = ?";
            if ($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }

        // Query for update
        $sql = "UPDATE {$table} SET {$set} WHERE uuid = {$id}";

        // If this doesn't error then return result.
        if (!$this->query($sql, $fields)->error()) {
            return true;
        }
        return false; // update unsuccessful
    }


    /**
     * Get if any error.
     * @return bool
     */
    public function error()
    {
        return $this->error;
    }


    /**
     * Get if any count of results.
     * @return int
     */
    public function count()
    {
        return $this->count;
    }
}