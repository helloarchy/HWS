<?php

/**
 * Class Validate
 * @author Robert Hardy
 * @since 2018-06-15
 *
 * This file defines the validation for inputs for the site. Check if errors, store
 * them and output them, and deal with link to database.
 */
class Validate
{
    private $passed = false,
        $errors = array(),
        $db = null;


    /**
     * Validate constructor.
     */
    public function __construct()
    {
        $this->db = Database::getInstance();
    }


    /**
     *
     * @param $source
     * @param array $items
     */
    public function check($source, $items = array())
    {
        // List through items and for each list through their rules against source
        foreach ($items as $item => $rules) {
            foreach ($rules as $rule => $ruleValue) {
                //echo "{$item} {$rule} must be {$ruleValue}</br>";
                $value = trim($source[$item]);
                $item = escape($item); // Security.

                // Check if required field has value
                if ($rule === 'required' && empty($value)) {
                    $this->addError("{$item} is required");
                    // TODO: Maybe something more informative?
                } else if (!empty($value)) {
                    //$value = $source[$item]; // Assign value now know its not empty.
                    // Switch the rules
                    switch ($rule) {
                        case 'min':
                            // Check length of value less than min
                            if (strlen($value) < $ruleValue) {
                                $this->addError("{$item} must be at least 
                                                        {$ruleValue} characters");
                            }
                            break;
                        case 'max':
                            // Check length of value more than max
                            if (strlen($value) > $ruleValue) {
                                $this->addError("{$item} must be less than 
                                                        {$ruleValue} characters");
                            }
                            break;
                        case 'matches':
                            // Check value matches rule (not in loop so use POST lookup for 'password')
                            if ($value != $source[$ruleValue]) {
                                $this->addError("{$ruleValue} must match {$item}");
                            }
                            break;
                        case 'unique':
                            // Check using database instance
                            $check = $this->db->get($ruleValue, array($item, '=', $value));
                            // If positive count we know something exists
                            if ($check->count()) {
                                $this->addError("{$item} already exists.");
                            }
                            break;
                        case 'type':
                            // Check the type of the value
                            switch ($ruleValue) {
                                case 'number':
                                    if (!is_numeric($value)) {
                                        $this->addError("{$item} is not numeric.");
                                    }
                                    break;
                                case 'image':
                                    /* todo: check if uploaded file is an image. */
                            }
                    }
                }
            }
        }
        // If no errors then passed!
        if (empty($this->errors)) {
            $this->passed = true;
        }
    }


    /**
     * Add a given error to the total list of errors
     * @param $error
     */
    private function addError($error)
    {
        $this->errors[] = $error;
    }


    /**
     * Get the list of errors
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }


    /**
     * Get if the validation passed
     * @return bool
     */
    public function getPassed()
    {
        return $this->passed;
    }
}