<?php

/**
 * Class Token
 * @author Robert Hardy
 * @since 2018-06-15
 *
 * This file defines a token for the site, used for security against cross-site
 * request forgery (CSRF), where someone may have the ability to execute unwanted
 * actions on a users page using POST/GET. To prevent against this, a token is
 * passed for validation, and deleted/updated as needed.
 */
class Token
{
    /**
     * Generates a token, an MD5 hash
     */
    public static function generate()
    {
        return Session::put(Config::get('session/token_name'), md5(uniqid()));
    }


    /**
     * Check if token already defined, if so, delete session and return true.
     * @param $token
     * @return bool
     */
    public static function check($token)
    {
        $tokenName = Config::get('session/token_name');
        // If session with the token exists, delete it.
        if (Session::exists($tokenName) && $token === Session::get($tokenName)) {
            Session::delete($tokenName);
            return true;
        }
        return false;
    }
}