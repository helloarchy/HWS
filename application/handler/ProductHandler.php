<?php
/**
 * ProductHandler.php
 * Handle ProductList.php ajax requests.
 *
 * @author Robert Hardy
 * @since 2018-08-16
 * Part of the Hardy'sWebSystem project.
 */
class ProductHandler
{
    private $db;
    private $sql;
    private $n_counter;


    /**
     * ProductHandler constructor.
     */
    public function __construct()
    {
        $this->db = Database::getInstance();
        if(Session::exists("product_sql")) {
            $this->sql = Session::get("product_sql");
        }
        if(Session::exists("product_n_counter")) {
            $this->n_counter = Session::get("product_n_counter");
        }
    }


    /**
     * Get the first batch of results.
     * @param $N
     * @param null $brand
     * @param null $collection
     * @param null $price_current
     * @param null $colour_hardys
     * @param null $width
     * @param null $pattern
     * @param null $type
     * @param null $application
     * @param null $sort
     * @return null|string
     */
    public function getFirst($N, $brand = null, $collection = null,
                             $price_current = null, $colour_hardys = null,
                             $width = null, $pattern = null, $type = null,
                             $application = null, $sort = null)
    {
        $n = (int)$N;
        $this->sql = "select product_fabric.ufid, reduction, title, brand, collection, 
                              price_current, price_original, colour_hardys, width, 
                              pattern, type, image_main
                    from (
                        (select ufid, (price_current - price_original) as reduction
                         from product_fabric order by reduction desc) as reductions
                        join product_fabric on product_fabric.ufid = reductions.ufid
                        )
                    where available = 1";

        /* Apply all filters in the nested WHERE statement */
        if (isset($brand)) {
            if (!empty($brand)) {
                self::appendChoices("brand", $brand);
            }
        }
        if (isset($collection)) {
            if (!empty($collection)) {
                self::appendChoices("collection", $collection);
            }
        }
        if (isset($colour_hardys)) {
            if (!empty($colour_hardys)) {
                self::appendChoices("colour_hardys", $colour_hardys);
            }
        }
        if (isset($width)) {
            if (!empty($width)) {
                self::appendChoices("width", $width);
            }
        }
        if (isset($pattern)) {
            if (!empty($pattern)) {
                self::appendChoices("pattern", $pattern);
            }
        }
        if (isset($type)) {
            if (!empty($type)) {
                self::appendChoices("type", $type);
            }
        }
        if (isset($application)) {
            if (!empty($application)) {
                self::appendChoices("application", $application);
            }
        }
        // Price filter in the outer WHERE statement
        if (isset($price_current)) {
            if (!empty($price_current)) {
                self::appendChoices("price_current", $price_current);
            }
        }
        // Finally append sort
        if (isset($sort)) {
            if (!empty($sort)) {
                $sort = json_decode($sort);
                $this->sql .= " order by {$sort[0]} {$sort[1]} ";
            }
        }

        /* build array from results if there are any, encode and return it. */
        $results = $this->db->getRows($this->sql . " limit 0, {$n};");
        $results_as_array = array();
        foreach ($results->results() as $row) {
            $results_as_array[] = $row;
        }

        /* Persist SQL and N_counter by keeping in Session */
        Session::put("product_sql", $this->sql);
        Session::put("product_n_counter", $N);

        return json_encode($results_as_array);
    }


    /**
     * Append the choices contained within an array to an SQL query.
     * @param $column
     * @param $choices
     */
    public function appendChoices($column, $choices)
    {
        /* Regex the current SQL to determine if the word "and" appears, if so
        use OR for the remaining filters. */
        $and_or = "and";
        if (!!preg_match('#\\b' . preg_quote("and", '#') .
            '\\b#i', $this->sql)) {
            $and_or = "or";
        }

        /* Append filters to the WHERE condition of the SQL. */
        $choices = json_decode($choices, true);
        if (!empty($choices)) {
            /* See if a range value, ie, price or width */
            if (!($column == "price_current")) {
                $length = count($choices);
                foreach ($choices as $index => $choice) {
                    if ($length == 1) {
                        // Only 1 item in array
                        $this->sql .= " {$and_or} ({$column} = '{$choice}') ";
                    } else if ($index == 0) {
                        // first
                        $this->sql .= " {$and_or} ({$column} = '{$choice}'";
                    } else if ($index == $length - 1) {
                        // last
                        $this->sql .= " or {$column} = '{$choice}') ";
                    } else {
                        // middle item(s) of array
                        $this->sql .= " or {$column} = '{$choice}'";
                    }
                }
            } else {
                /* Finally add the price by nesting current query
                inside another select */
                $nested_query = "select * from ( " . $this->sql . " ) as filtered";
                $nested_query .= " where {$column} <= {$choices}";
                $this->sql = $nested_query;
            }
        }
    }


    /**
     * @param $N
     * @return mixed
     */
    public function getNext($N)
    {
        $offset = $N;
        if(Session::exists("product_n_counter")) {
            $offset = Session::get("product_n_counter");
        }

        $results = $this->db->getRows($this->sql .
            " limit {$N} offset {$offset};");
        $results_as_array = array();
        foreach ($results->results() as $row) {
            $results_as_array[] = $row;
        }

        /* Move offset */
        Session::put("product_n_counter", $N + $offset);

        return json_encode($results_as_array);
    }



    /**
     * @param $N
     * @return bool
     */
    public function hasNext($N)
    {
        $offset = $N;
        if(Session::exists("product_n_counter")) {
            $offset = Session::get("product_n_counter");
        }

        $results = $this->db->getRows($this->sql .
            " limit {$N} offset {$offset};");
        $results_as_array = array();
        foreach ($results->results() as $row) {
            $results_as_array[] = $row;
        }

        if (count($results_as_array) > 0) {
            return json_encode(true);
        }
        return json_encode(false);
    }
}

